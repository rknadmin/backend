FROM node:18-alpine

WORKDIR /usr/src/app

COPY . .

RUN npm ci && npm cache clean --force
RUN npm run build
ENV NODE_ENV production

CMD [ "npm", "start", "prod" ]

EXPOSE ${PORT}
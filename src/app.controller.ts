import { Controller, Get, Param, Render, UseGuards } from "@nestjs/common";
import { AppService } from "./app.service";
import { AuthenticatedGuard } from "./common/guards/authenticated.guard";
import { User } from "./common/decorators/user.decorator";
import { User as UserEntity } from "./users/user.entity";

@Controller()
export class AppController {
    constructor(private readonly appService: AppService) {
    }

    @Get('admin/privacy')
    @Render('privacy')
    async privacy() {
        return;
    }

    @Get()
    @Render('home/pages/index')
    async index() {
        return {title: 'Strona główna'};
    }

    @Get('kola-i-organizacje')
    @Render('home/pages/societies')
    async societies() {
        const [organizations, faculties] = await this.appService.getFaculties();
        return {
            title: 'Koła i Organizacje',
            organizations,
            faculties,
        };
    }

    @Get('wydarzenia')
    @Render('home/pages/events')
    async events() {
        const events = await this.appService.getEvents();
        return {title: 'Wydarzenia', events};
    }

    @Get('dokumenty')
    @Render('home/pages/docs')
    documents() {
        return {title: 'Dokumenty'};
    }

    @Get('faq')
    @Render('home/pages/faq')
    async faq() {
        const categories = await this.appService.getFaqCategories();
        return {title: 'FAQ', categories};
    }

    @Get('kontakt')
    @Render('home/pages/contact')
    contact() {
        return {title: 'Kontakt'};
    }

    @Get('kola-i-organizacje/:slug')
    @Render('home/pages/society')
    async society(@Param('slug') slug: string) {
        const society = await this.appService.getSociety(slug);
        return {title: society.name, society};
    }

    @UseGuards(AuthenticatedGuard)
    @Get('/home')
    @Render('dashboard/pages/home')
    getHome(@User() user: UserEntity) {
        return {title: 'Strona główna', user};
    }
}

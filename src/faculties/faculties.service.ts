import { Injectable, InternalServerErrorException } from "@nestjs/common";
import { CreateFacultyDto } from "./dto/create-faculty.dto";
import { UpdateFacultyDto } from "./dto/update-faculty.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Faculty } from "./faculty.entity";
import { Repository } from "typeorm";

@Injectable()
export class FacultiesService {
    constructor(
        @InjectRepository(Faculty) private facultiesRepository: Repository<Faculty>,
    ) {
    }

    async create(createFacultyDto: CreateFacultyDto) {
        const faculty = this.facultiesRepository.create(createFacultyDto);
        return this.saveHelper(faculty);
    }

    async findAll() {
        return await this.facultiesRepository.createQueryBuilder('faculty')
            .leftJoinAndSelect('faculty.societies', 'society')
            .orderBy('faculty.name', 'ASC')
            .addOrderBy('society.name', 'ASC')
            .getMany();
    }

    async findOne(id: number) {
        return await this.facultiesRepository.createQueryBuilder('faculty')
            .where('faculty.id = :id', {id})
            .leftJoinAndSelect('faculty.societies', 'society')
            .orderBy('society.name')
            .getOne();
    }

    async findBySlug(slug: string) {
        return await this.facultiesRepository.createQueryBuilder('faculty')
            .where('faculty.slug = :slug', {slug})
            .leftJoinAndSelect('faculty.societies', 'society')
            .orderBy('society.name')
            .getOne();
    }

    async update(id: number, updateFacultyDto: UpdateFacultyDto) {
        const faculty = await this.findOne(id);
        if (faculty.name === 'Organizacje Studenckie')
            delete updateFacultyDto.name;
        for (const prop in updateFacultyDto) faculty[prop] = updateFacultyDto[prop];
        return this.saveHelper(faculty);
    }

    async remove(id: number) {
        return await this.facultiesRepository.delete(id);
    }

    async saveHelper(faculty: Faculty) {
        try {
            return await faculty.save();
        } catch (err) {
            if (err.errno === 1062)
                throw new InternalServerErrorException(
                    `Faculty with name "${faculty.name}" or slug "${faculty.slug}" already exists.`,
                );
            else throw new InternalServerErrorException();
        }
    }
}

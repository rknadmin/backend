import { Body, Controller, Delete, Get, Param, Post, Put, UseFilters, UseGuards } from "@nestjs/common";
import { FacultiesService } from "./faculties.service";
import { CreateFacultyDto } from "./dto/create-faculty.dto";
import { UpdateFacultyDto } from "./dto/update-faculty.dto";
import { AuthExceptionFilter } from "../common/filters/auth-exceptions.filter";
import { AuthenticatedGuard } from "../common/guards/authenticated.guard";
import { Roles } from "../common/decorators/roles.decorator";

@Controller('api/faculties')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class FacultiesController {
    constructor(private readonly facultiesService: FacultiesService) {
    }

    @Post()
    @Roles('rkn', 'zco')
    create(@Body() createFacultyDto: CreateFacultyDto) {
        return this.facultiesService.create(createFacultyDto);
    }

    @Get()
    @Roles('rkn', 'zco')
    async findAll() {
        return await this.facultiesService.findAll();
    }

    @Get(':id')
    @Roles('rkn', 'zco')
    async findOne(@Param('id') id: string) {
        return await this.facultiesService.findOne(+id);
    }

    @Put(':id')
    @Roles('rkn', 'zco')
    async update(
        @Param('id') id: string,
        @Body() updateFacultyDto: UpdateFacultyDto,
    ) {
        return await this.facultiesService.update(+id, updateFacultyDto);
    }

    @Delete(':id')
    @Roles('rkn', 'zco')
    async remove(@Param('id') id: string) {
        return await this.facultiesService.remove(+id);
    }
}

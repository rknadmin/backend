import { Module } from '@nestjs/common';
import { DashboardController } from './dashboard.controller';
import { DashboardService } from './dashboard.service';
import { FacultiesModule } from '../faculties/faculties.module';
import { SocietiesModule } from '../societies/societies.module';
import { EventsModule } from '../events/events.module';
import { FaqCategoriesModule } from '../faq/faq-categories/faq-categories.module';
import { FaqQuestionsModule } from '../faq/faq-questions/faq-questions.module';
import { UsersModule } from '../users/users.module';
import { AccountsModule } from '../accounts/accounts.module';
import { FundingsModule } from '../fundings/fundings.module';
import { APP_GUARD } from '@nestjs/core';
import { RolesGuard } from '../common/guards/roles.guard';
import { DashboardUsersController } from './dashboard-users.controller';
import { DashboardUsersService } from './dashboard-users.service';
import { DashboardZcoController } from './dashboard-zco.controller';
import { DashboardZcoService } from './dashboard-zco.service';
import { GrantsModule } from '../grants/grants.module';
import { BudgetsModule } from '../budgets/budgets.module';
import { DashboardRknController } from './dashboard-rkn.controller';
import { DashboardRknService } from './dashboard-rkn.service';
import { FilesModule } from '../files/files.module';

@Module({
    controllers: [DashboardController, DashboardUsersController, DashboardZcoController, DashboardRknController],
    providers: [DashboardService, DashboardUsersService, DashboardZcoService, DashboardRknService, {
        provide: APP_GUARD,
        useClass: RolesGuard,
    },],
    imports: [FilesModule, EventsModule, FacultiesModule, FaqCategoriesModule, FaqQuestionsModule, SocietiesModule, UsersModule, AccountsModule, FundingsModule, GrantsModule, BudgetsModule]
})
export class DashboardModule {
}

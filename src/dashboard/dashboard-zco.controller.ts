import { Controller, Get, Param, Render, UseFilters, UseGuards } from '@nestjs/common';
import { AuthExceptionFilter } from '../common/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '../common/guards/authenticated.guard';
import { Roles } from '../common/decorators/roles.decorator';
import { User } from '../common/decorators/user.decorator';
import { User as UserEntity } from '../users/user.entity';
import { DashboardZcoService } from './dashboard-zco.service';

@Controller('admin/zco')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class DashboardZcoController {
    constructor(private dashboardZcoService: DashboardZcoService) {
    }

    @Get('accounts')
    @Roles('zco')
    @Render('dashboard/pages/zco/accounts/index')
    async accounts(@User() user: UserEntity) {
        const accounts = await this.dashboardZcoService.getAccounts();
        return {title: 'Konta', accounts, user};
    }

    @Get('accounts/new')
    @Roles('zco')
    @Render('dashboard/pages/zco/accounts/add')
    async addAccount(@User() user: UserEntity) {
        const societies = await this.dashboardZcoService.getSocieties();
        return {title: 'Nowe konto', societies, user};
    }

    @Get('accounts/:id')
    @Roles('zco')
    @Render('dashboard/pages/zco/accounts/show')
    async account(@User() user: UserEntity, @Param('id') id: string) {
        const account = await this.dashboardZcoService.getAccount(+id);
        return {title: `Wnioski konta ${account.name}`, user, account};
    }

    @Get('accounts/:id/distribute')
    @Roles('zco')
    @Render('dashboard/pages/zco/accounts/distribute')
    async distributeAccount(@User() user: UserEntity, @Param('id') id: string) {
        const account = await this.dashboardZcoService.getAccount(+id);
        return {title: `Edycja konta ${account.name}`, user, account};
    }

    @Get('accounts/:id/edit')
    @Roles('zco')
    @Render('dashboard/pages/zco/accounts/edit')
    async editAccount(@User() user: UserEntity, @Param('id') id: string) {
        const account = await this.dashboardZcoService.getAccount(+id);
        const societies = await this.dashboardZcoService.getSocieties();
        return {title: account.name, account, societies, user};
    }

    @Get('fundings')
    @Roles('zco')
    @Render('dashboard/pages/zco/fundings/index')
    async fundings(@User() user: UserEntity) {
        const fundings = await this.dashboardZcoService.getFundings();
        const societies = await this.dashboardZcoService.findAllWithBudgets();
        return {title: 'Wnioski o finansowanie', fundings, societies, user};
    }

    @Get('fundings/new/:slug')
    @Roles('zco')
    @Render('dashboard/pages/zco/fundings/new')
    async addFunding(@User() user: UserEntity, @Param('slug') slug: string) {
        const society = await this.dashboardZcoService.getSociety(slug);
        return {title: 'Nowy wniosek o finansowanie', society, user};
    }

    @Get('grants')
    @Roles('zco')
    @Render('dashboard/pages/zco/grants/index')
    async grants(@User() user: UserEntity) {
        const grants = await this.dashboardZcoService.getGrants();
        const societies = await this.dashboardZcoService.findAllWithBudgets();
        return {title: 'Wnioski grantowe', grants, societies, user};
    }

    @Get('grants/new/:slug')
    @Roles('zco')
    @Render('dashboard/pages/zco/grants/new')
    async addGrant(@User() user: UserEntity, @Param('slug') slug: string) {
        const society = await this.dashboardZcoService.getSociety(slug);
        return {title: 'Nowy wniosek grantowy', society, user};
    }
}

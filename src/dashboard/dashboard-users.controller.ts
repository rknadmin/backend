import { Controller, Get, Param, Render, UseFilters, UseGuards } from '@nestjs/common';
import { AuthExceptionFilter } from '../common/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '../common/guards/authenticated.guard';
import { DashboardUsersService } from './dashboard-users.service';
import { Roles } from '../common/decorators/roles.decorator';
import { User } from '../common/decorators/user.decorator';
import { User as UserEntity } from '../users/user.entity';

@Controller('admin')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class DashboardUsersController {
    constructor(private dashboardUsersService: DashboardUsersService) {
    }

    @Get('societies/:slug/edit')
    @Roles('all')
    @Render('dashboard/pages/users/edit')
    async societyEdit(@User() user: UserEntity, @Param('slug') slug: string) {
        const society = await this.dashboardUsersService.getSociety(slug);
        return {title: society.name, slug: society.slug, society, user}
    }

    @Get('societies/:slug/budgets')
    @Roles('all')
    @Render('dashboard/pages/users/budgets')
    async societyAccounts(@User() user: UserEntity, @Param('slug') slug: string) {
        const society = await this.dashboardUsersService.getSociety(slug);
        return {title: society.name, slug: society.slug, society, user}
    }

    @Get('societies/:slug/fundings')
    @Roles('all')
    @Render('dashboard/pages/users/fundings')
    async societyFundings(@User() user: UserEntity, @Param('slug') slug: string) {
        const society = await this.dashboardUsersService.getSociety(slug);
        return { title: society.name, slug: society.slug, society, user };
    }

    @Get('societies/:slug/grants/:id/edit')
    @Roles('all')
    @Render('dashboard/pages/users/grant-edit')
    async societyFundingEdit(@User() user: UserEntity, @Param('slug') slug: string, @Param('id') id: string) {
        const society = await this.dashboardUsersService.getSociety(slug);
        const funding = await this.dashboardUsersService.getFunding(+id);
        const files = await this.dashboardUsersService.getGrantFiles(+id);
        return {
            title: `Edycja wniosku o finansowanie: ${funding.subject}`,
            slug: society.slug,
            society,
            funding,
            files,
            user
        };
    }

    @Get('societies/:slug/grants')
    @Roles('all')
    @Render('dashboard/pages/users/grants')
    async societyGrants(@User() user: UserEntity, @Param('slug') slug: string) {
        const society = await this.dashboardUsersService.getSociety(slug);
        return { title: society.name, slug: society.slug, society, user };
    }

    @Get('societies/:slug/grants/:id/edit')
    @Roles('all')
    @Render('dashboard/pages/users/grant-edit')
    async societyGrantEdit(@User() user: UserEntity, @Param('slug') slug: string, @Param('id') id: string) {
        const society = await this.dashboardUsersService.getSociety(slug);
        const grant = await this.dashboardUsersService.getGrant(+id);
        const files = await this.dashboardUsersService.getGrantFiles(+id);
        return { title: `Edycja wniosku grantowego: ${grant.subject}`, slug: society.slug, society, grant, files, user };
    }

}

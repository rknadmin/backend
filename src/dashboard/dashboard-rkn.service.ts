import {Inject, Injectable} from "@nestjs/common";
import { SocietiesService } from "../societies/societies.service";
import { FacultiesService } from "../faculties/faculties.service";
import { FaqCategoriesService } from "../faq/faq-categories/faq-categories.service";
import { UsersService } from "../users/users.service";
import { GrantsService } from "../grants/grants.service";
import {WINSTON_MODULE_NEST_PROVIDER} from "nest-winston";
import {Faculty} from "../faculties/faculty.entity";
import {Society} from "../societies/society.entity";
import {User} from "../users/user.entity";
import {Grant} from "../grants/grant.entity";


@Injectable()
export class DashboardRknService {
    constructor(
        private facultiesService: FacultiesService,
        private faqCategoriesService: FaqCategoriesService,
        private societiesService: SocietiesService,
        private usersService: UsersService,
        private grantsService: GrantsService,
        @Inject(WINSTON_MODULE_NEST_PROVIDER) private readonly logger
    ){
        this.logger.setContext(DashboardRknService.name);
    }

    async getFaculties(): Promise<Faculty[]> {
        return await this.facultiesService.findAll();
    }

    async getFaculty(slug): Promise<Faculty> {
        return await this.facultiesService.findBySlug(slug);
    }

    async getSocieties(): Promise<Society[]> {
        return await this.societiesService.findAll();
    }

    async getSociety(slug: string): Promise<Society> {
        return await this.societiesService.findBySlug(slug);
    }

    async getUsers(): Promise<User[]> {
        return await this.usersService.findAll();
    }

    async getUser(id?: number): Promise<User> {
        if (id) return await this.usersService.findOne(id);
        else return await this.usersService.getThis();
    }

    async getGrants(): Promise<Grant[]> {
        return await this.grantsService.findAll();
    }
}

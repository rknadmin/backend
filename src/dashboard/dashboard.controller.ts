import {
  Body,
  Controller,
  Get,
  Param,
  Post,
  Render,
  UploadedFiles,
  UseFilters,
  UseGuards,
  UseInterceptors
} from '@nestjs/common';
import { DashboardService } from './dashboard.service';
import { FilesInterceptor } from '@nestjs/platform-express';
import { AuthenticatedGuard } from '../common/guards/authenticated.guard';
import { AuthExceptionFilter } from '../common/filters/auth-exceptions.filter';
import { User } from '../common/decorators/user.decorator';
import { User as UserEntity } from '../users/user.entity';
import { Roles } from '../common/decorators/roles.decorator';

@Controller('admin')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class DashboardController {
    constructor(private dashboardService: DashboardService) {
    }

    @Get()
    @Render('dashboard/pages/index')
    async index(@User() user: UserEntity) {
        const data = await this.dashboardService.getAll();
        return {title: 'Strona główna', data, user};
    }

    @Get('fundings/:id')
    @Roles('all')
    @Render('dashboard/pages/funding')
    async funding(@User() user: UserEntity, @Param('id') id: string) {
        const funding = await this.dashboardService.getFunding(+id);
        const files = await this.dashboardService.getFundingFiles(+id);
        return {title: `Wniosek o finansowanie: ${funding.subject}`, funding, files, user};
    }

    @Get('fundings/:id/edit')
    @Roles('rkn', 'zco')
    @Render('dashboard/pages/funding-edit')
    async fundingEdit(@User() user: UserEntity, @Param('id') id: string) {
        const funding = await this.dashboardService.getFunding(+id);
        const files = await this.dashboardService.getFundingFiles(+id);
        return {title: `Edycja wniosku o finansowanie: ${funding.subject}`, funding, files, user};
    }

    @Get('grants/:id')
    @Roles('all')
    @Render('dashboard/pages/grant')
    async grant(@User() user: UserEntity, @Param('id') id: string) {
        const grant = await this.dashboardService.getGrant(+id);
        const files = await this.dashboardService.getGrantFiles(+id);
        return {title: `Wniosek grantowy: ${grant.subject}`, grant, files, user};
    }

    @Get('grants/:id/edit')
    @Roles('all')
    @Render('dashboard/pages/grant-edit')
    async grantEdit(@User() user: UserEntity, @Param('id') id: string) {
        const grant = await this.dashboardService.getGrant(+id);
        const files = await this.dashboardService.getGrantFiles(+id);
        return {title: `Edycja wniosku grantowego: ${grant.subject}`, grant, files, user};
    }

    @Post('dummy')
    @UseInterceptors(FilesInterceptor('files', 10, {
        limits: {fileSize: 5000000},
        fileFilter: (req, file, cb) => {
            if (file.mimetype !== 'application/pdf' && !file.mimetype.startsWith('image/'))
                return cb(null, false)
            cb(null, true);
        }
    }))
    async dummy(@UploadedFiles() files: Array<Express.Multer.File>, @Body() body) {
        console.log(files);
        console.log(body);
        return 200;
    }
}

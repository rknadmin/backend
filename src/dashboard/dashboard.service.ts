import {Inject, Injectable} from '@nestjs/common';
import {FacultiesService} from '../faculties/faculties.service';
import {SocietiesService} from '../societies/societies.service';
import {FaqCategoriesService} from '../faq/faq-categories/faq-categories.service';
import {EventsService} from '../events/events.service';
import {UsersService} from '../users/users.service';
import {AccountsService} from '../accounts/accounts.service';
import {FundingsService} from '../fundings/fundings.service';
import {GrantsService} from '../grants/grants.service';
import {FilesService} from '../files/files.service';
import EntityType from '../files/enums/entity-type.enum';
import {WINSTON_MODULE_NEST_PROVIDER} from "nest-winston";
import {Funding} from "../fundings/funding.entity";
import {File} from "../files/file.entity";
import {Grant} from "../grants/grant.entity";

@Injectable()
export class DashboardService {
    constructor(
        private eventsService: EventsService,
        private facultiesService: FacultiesService,
        private faqCategoriesService: FaqCategoriesService,
        private societiesService: SocietiesService,
        private usersService: UsersService,
        private fundingsService: FundingsService,
        private grantsService: GrantsService,
        private filesService: FilesService,
        @Inject(WINSTON_MODULE_NEST_PROVIDER) private readonly logger
    ) {
        this.logger.setContext(DashboardService.name)
    }

    /**
     * Retrieves all data for the dashboard
     */
    async getAll() {
        const users = await this.usersService.findAll();
        const events = await this.eventsService.findAll();
        const faculties = await this.facultiesService.findAll();
        const faqCategories = await this.faqCategoriesService.findAll();
        const societies = await this.societiesService.findAll();
        const grants = await this.grantsService.findAll();
        const fundings = await this.fundingsService.findAll();
        return {
            users,
            events,
            faculties,
            faqCategories,
            societies,
            grants,
            fundings
        };
    }

    /**
     * Retrieves the Funding with the given id
     * @param id - The id of the Funding
     * @return The Funding with the given id
     */
    async getFunding(id: number): Promise<Funding> {
        return await this.fundingsService.findOne(id);
    }

    /**
     * Retrieves the files of the Funding with the given id
     * @param id - The id of the Funding
     * @return The files of the Funding with the given id
     */
    async getFundingFiles(id: number): Promise<File[]> {
        return await this.filesService.findFilesByEntity(EntityType.Funding, id);
    }

    /**
     * Retrieves the files of the Grant with the given id
     * @param id - The id of the Grant
     * @return The files of the Grant with the given id
     */
    async getGrantFiles(id: number): Promise<File[]> {
        return await this.filesService.findFilesByEntity(EntityType.Grant, id);
    }

    /**
     * Retrieves the grant with the given id
     * @param id - The id of the Grant
     * @return The grant with the given id
     */
    async getGrant(id: number): Promise<Grant> {
        return await this.grantsService.findOne(id);
    }
}

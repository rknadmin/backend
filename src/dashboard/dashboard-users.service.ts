import {Inject, Injectable} from '@nestjs/common';
import { SocietiesService } from '../societies/societies.service';
import { GrantsService } from '../grants/grants.service';
import EntityType from '../files/enums/entity-type.enum';
import { FilesService } from '../files/files.service';
import {WINSTON_MODULE_NEST_PROVIDER} from "nest-winston";
import {Society} from "../societies/society.entity";
import {Grant} from "../grants/grant.entity";
import {Funding} from "../fundings/funding.entity";
import {File} from "../files/file.entity";
import {FundingsService} from "../fundings/fundings.service";


@Injectable()
export class DashboardUsersService {
    constructor(
        private societiesService: SocietiesService,
        private grantsService: GrantsService,
        private fundingsService: FundingsService,
        private filesService: FilesService,
        @Inject(WINSTON_MODULE_NEST_PROVIDER) private readonly logger
    ) {
        this.logger.setContext(DashboardUsersService.name);
    }

    async getSociety(slug: string): Promise<Society> {
        return await this.societiesService.findBySlug(slug);
    }

    async getGrant(id: number): Promise<Grant> {
        return await this.grantsService.findOne(id);
    }

    async getFunding(id: number): Promise<Funding> {
        return await this.fundingsService.findOne(id);
    }

    async getGrantFiles(id: number):Promise<File[]> {
        return await this.filesService.findFilesByEntity(EntityType.Grant, id);
    }
}

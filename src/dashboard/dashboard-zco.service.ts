import {Inject, Injectable} from '@nestjs/common';
import {AccountsService} from '../accounts/accounts.service';
import {SocietiesService} from '../societies/societies.service';
import {GrantsService} from '../grants/grants.service';
import {FundingsService} from '../fundings/fundings.service';
import {BudgetsService} from '../budgets/budgets.service';
import {WINSTON_MODULE_NEST_PROVIDER} from "nest-winston";
import {Account} from "../accounts/account.entity";
import {Society} from "../societies/society.entity";
import {Funding} from "../fundings/funding.entity";
import {Grant} from "../grants/grant.entity";


@Injectable()
export class DashboardZcoService {
  constructor(
    private accountsService: AccountsService,
    private societiesService: SocietiesService,
    private fundingsService: FundingsService,
    private grantsService: GrantsService,
    @Inject(WINSTON_MODULE_NEST_PROVIDER) private readonly logger
    ) {
      this.logger.setContext(DashboardZcoService.name)
    }

    /**
     * Retrieves all accounts from the database.
     */
    async getAccounts(): Promise<Account[]> {
        return await this.accountsService.findAll();
    }

    /**
     * Retrieves a single account from the database.
     * @param id The id of the account to retrieve.
     */
    async getAccount(id: number): Promise<Account> {
        return await this.accountsService.findOne(id);
    }

    /**
     * Retrieves all societies from the database.
     */
    async getSocieties(): Promise<Society[]> {
        return await this.societiesService.findAll();
    }

    /**
     * Retrieves a single society from the database.
     * @param slug - The slug of the society to retrieve.
     */
    async getSociety(slug: string): Promise<Society> {
        return await this.societiesService.findBySlug(slug);
    }

    /**
     * Retrieves all societies from the database with their budgets.
     */
    async findAllWithBudgets(): Promise<Society[]> {
        return await this.societiesService.findAllWithBudgets();
    }

    /**
     * Retrieves all fundings from the database.
     */
    async getFundings(): Promise<Funding[]> {
        return await this.fundingsService.findAll();
    }

    /**
     * Retrieves all grants from the database.
     */
    async getGrants(): Promise<Grant[]> {
        return await this.grantsService.findAll();
    }
}

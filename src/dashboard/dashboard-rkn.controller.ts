import { Controller, Get, Param, Render, Res, UseFilters, UseGuards } from '@nestjs/common';
import { AuthExceptionFilter } from '../common/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '../common/guards/authenticated.guard';
import { DashboardRknService } from './dashboard-rkn.service';
import { Roles } from '../common/decorators/roles.decorator';
import { User } from '../common/decorators/user.decorator';
import { User as UserEntity } from '../users/user.entity';
import { Response } from 'express';

@Controller('admin/rkn')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class DashboardRknController {
    constructor(private dashboardRknService: DashboardRknService) {
    }

    @Get('societies/new')
    @Roles('rkn', 'zco')
    @Render('dashboard/pages/rkn/societies/new')
    async addSociety(@User() user: UserEntity) {
        const faculties = await this.dashboardRknService.getFaculties();
        return {title: 'Dodaj KN/OS', faculties, user}
    }

    @Get('societies/:slug/edit')
    @Roles('all')
    @Render('dashboard/pages/rkn/societies/edit')
    async editSociety(@User() user: UserEntity, @Param('slug') slug: string) {
        const society = await this.dashboardRknService.getSociety(slug);
        const faculties = await this.dashboardRknService.getFaculties();
        return {title: society.name, faculties, society, user}
    }

    @Get('societies')
    @Roles('rkn', 'zco')
    @Render('dashboard/pages/rkn/societies/index')
    async societies(@User() user: UserEntity) {
        const societies = await this.dashboardRknService.getSocieties();
        return {title: 'Koła Naukowe i Organizacje Studenckie', societies, user}
    }

    @Get('faculties')
    @Roles('rkn', 'zco')
    @Render('dashboard/pages/rkn/faculties/index')
    async faculties(@User() user: UserEntity) {
        const faculties = await this.dashboardRknService.getFaculties();
        return {title: 'Wydziały', faculties, user}
    }

    @Get('faculties/new')
    @Roles('rkn', 'zco')
    @Render('dashboard/pages/rkn/faculties/new')
    async addFaculty(@User() user: UserEntity) {
        return {title: 'Dodaj wydział', user}
    }

    @Get('faculties/:slug/edit')
    @Roles('rkn', 'zco')
    @Render('dashboard/pages/rkn/faculties/edit')
    async editFaculty(@User() user: UserEntity, @Param('slug') slug: string, @Res() res: Response) {
        if (slug === 'organizacje-studenckie')
            return res.redirect('faculties');
        else {
            const faculty = await this.dashboardRknService.getFaculty(slug);
            return {title: faculty.name, faculty, user}
        }
    }

    @Get('users')
    @Roles('rkn', 'zco')
    @Render('dashboard/pages/rkn/users/index')
    async users(@User() user: UserEntity) {
        const users = await this.dashboardRknService.getUsers();
        return {title: 'Użytkownicy', users, user}
    }

    @Get('users/new')
    @Roles('rkn', 'zco')
    @Render('dashboard/pages/rkn/users/new')
    async addUser(@User() user: UserEntity) {
        const societies = await this.dashboardRknService.getSocieties();
        return {title: 'Dodaj użytkownika', societies, user}
    }

    @Get('users/:id/edit')
    @Roles('rkn', 'zco')
    @Render('dashboard/pages/rkn/users/edit')
    async editUser(@User() user: UserEntity, @Param('id') id: string) {
        const societies = await this.dashboardRknService.getSocieties();
        const editUser = await this.dashboardRknService.getUser(+id);
        return {title: user.name, societies, user, editUser};
    }

    @Get('grants')
    @Roles('rkn', 'zco')
    @Render('dashboard/pages/grants')
    async grants(@User() user: UserEntity) {
        const grants = await this.dashboardRknService.getGrants();
        return {title: 'Wnioski grantowe', grants, user};
    }
}

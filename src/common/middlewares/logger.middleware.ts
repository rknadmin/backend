import {Inject, NestMiddleware} from '@nestjs/common';
import {WINSTON_MODULE_NEST_PROVIDER} from 'nest-winston';
import {NextFunction, Request, Response} from 'express';

export class LoggerMiddleware implements NestMiddleware {
    @Inject(WINSTON_MODULE_NEST_PROVIDER) logger

    use(request: Request, response: Response, next: NextFunction): void {
        const { method, path: url, body } = request;
        const user = request['user']

        response.on('close', () => {
            const { statusCode } = response;
            let message = `${method} ${url} ${statusCode}`
            if (user) message += ` - User: ${user.email}`
            if (Object.keys(body).length > 0) message += ` - Body: ${JSON.stringify(body)}`

            this.logger.debug(message);
        });

        next();
    }
}
import { BeforeInsert, BeforeUpdate, Column, Entity } from 'typeorm';
import { TimeStampedEntity } from './time-stamped.entity';
import slugify from 'slugify';
import { IsNotEmpty } from 'class-validator';

@Entity()
export abstract class TimeStampedSluggifiedEntity extends TimeStampedEntity {
    @Column({unique: true})
    @IsNotEmpty()
    name: string;

    @Column({unique: true})
    @IsNotEmpty()
    slug: string;

    @BeforeInsert()
    @BeforeUpdate()
    updateSlug() {
        this.slug = slugify(this.name, {lower: true, remove: /[*+~.()''!:@]/g, locale: 'pl'});
        this.slug = this.slug.replace(/\//g, '-');
    }
}

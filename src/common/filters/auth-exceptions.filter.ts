import {
    ArgumentsHost,
    Catch,
    ExceptionFilter,
    ForbiddenException,
    HttpException,
    UnauthorizedException
} from '@nestjs/common';
import { Response, Request } from 'express';

@Catch(HttpException)
export class AuthExceptionFilter implements ExceptionFilter {
    catch(exception: HttpException, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse<Response>();
        const request = ctx.getRequest<Request>();

        // Handle Unauthorized and Forbidden separately for GET and non-GET requests.
        if (exception instanceof ForbiddenException) {
            // Check if it's a non-GET request (like POST, PATCH, DELETE, etc.)
            if (request.method !== 'GET') {
                return response.status(403).json({
                    statusCode: 403,
                    message: 'Forbidden: You don\'t have permission to perform this action.'
                });
            }
            return response.redirect('/auth');
        } else if (exception instanceof UnauthorizedException) {
            if (request.method !== 'GET') {
                return response.status(401).json({
                    statusCode: 401,
                    message: 'Unauthorized: Your session may have expired or you might not have access to this resource.'
                });
            }
            return response.redirect('/admin');
        } else {
            // Default behavior for other types of HttpExceptions
            return response.status(exception.getStatus()).json({
                statusCode: exception.getStatus(),
                message: exception.message
            });
        }
    }
}

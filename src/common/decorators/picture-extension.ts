import {
  registerDecorator,
  ValidationArguments,
  ValidatorConstraint,
  ValidatorConstraintInterface
} from 'class-validator';

@ValidatorConstraint({async: false})
class IsAcceptedPictureConstraint implements ValidatorConstraintInterface {
    validate(text: string): boolean {
        const extension = text.split('.')[1];
        return (
            extension === 'png' ||
            extension === 'jpg' ||
            extension === 'svg'
        );
    }

    defaultMessage(args: ValidationArguments): string {
        return `Accepted picture extensions are: '.png', '.jpg', '.svg'. Sent: '${args.value.split('.')[1]}'.`;
    }
}

export function IsAcceptedPicture(): PropertyDecorator {
    return (object: { [key: string]: any }, propertyName: string) => {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            validator: IsAcceptedPictureConstraint,
        });
    };
}

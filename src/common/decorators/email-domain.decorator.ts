import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface
} from 'class-validator';

@ValidatorConstraint({async: true})
export class IsPutDomainConstraint implements ValidatorConstraintInterface {
    validate(email: string, args: ValidationArguments): boolean {
        if (!email) return true;

        const [_, domain] = email.split('@');
        if (
            domain === 'put.poznan.pl' ||
            domain === 'student.put.poznan.pl' ||
            domain === 'rkn.put.poznan.pl' ||
            domain === 'doctorate.put.poznan.pl'
        ) return true;
        else throw new Error(`Email '${email}' is not a PUT domain email!`);
    }
}

export function IsPutDomain(validationOptions?: ValidationOptions) {
    return function (object: Object, propertyName: string): void {
        registerDecorator({
            target: object.constructor,
            propertyName: propertyName,
            options: validationOptions,
            constraints: [],
            validator: IsPutDomainConstraint,
        });
    };
}
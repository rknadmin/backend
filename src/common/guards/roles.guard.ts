import {CanActivate, ExecutionContext, Injectable, UnauthorizedException} from '@nestjs/common';
import {Reflector} from '@nestjs/core';

@Injectable()
export class RolesGuard implements CanActivate {
    constructor(private reflector: Reflector) {
    }

    canActivate(context: ExecutionContext): boolean {
        const roles = this.reflector.get<string[]>('roles', context.getHandler());

        if (!roles)
            return true;

        const request = context.switchToHttp().getRequest();
        const user = request.user;

        if (user && (roles.some(role => user.role === role) || roles.includes('all'))) {
            if (request.params && (request.params.slug || request.params.id)) {
                if (
                    user.societies.some(society => society.slug === request.params.slug)
                    ||
                    user.societies.some(society => society.id === +request.params.id)
                    ||
                    ['rkn', 'zco'].includes(user.role)
                ) return true;
                else {
                    throw new UnauthorizedException();
                }
            }
            return true;
        }
        throw new UnauthorizedException();
    }
}

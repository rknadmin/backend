import { Module } from "@nestjs/common";
import { FaqQuestionsService } from "./faq-questions.service";
import { FaqQuestionsController } from "./faq-questions.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { FaqQuestion } from "./faq-question.entity";

@Module({
    imports: [TypeOrmModule.forFeature([FaqQuestion])],
    controllers: [FaqQuestionsController],
    providers: [FaqQuestionsService],
})
export class FaqQuestionsModule {
}

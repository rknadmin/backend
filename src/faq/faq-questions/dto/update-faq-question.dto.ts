import { PartialType } from "@nestjs/mapped-types";
import { CreateFaqQuestionDto } from "./create-faq-question.dto";

export class UpdateFaqQuestionDto extends PartialType(CreateFaqQuestionDto) {
    name?: string;
    answer?: string;
}

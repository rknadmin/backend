import { IsNotEmpty } from "class-validator";

export class CreateFaqQuestionDto {
    @IsNotEmpty()
    name!: string;

    @IsNotEmpty()
    answer!: string;
}

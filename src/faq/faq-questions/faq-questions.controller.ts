import { Body, Controller, Delete, Get, Param, Patch, Post, UseFilters, UseGuards } from "@nestjs/common";
import { FaqQuestionsService } from "./faq-questions.service";
import { CreateFaqQuestionDto } from "./dto/create-faq-question.dto";
import { UpdateFaqQuestionDto } from "./dto/update-faq-question.dto";
import { AuthExceptionFilter } from "../../common/filters/auth-exceptions.filter";
import { AuthenticatedGuard } from "../../common/guards/authenticated.guard";
import { Roles } from "../../common/decorators/roles.decorator";

@Controller('api/faq-questions')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class FaqQuestionsController {
    constructor(private readonly faqQuestionsService: FaqQuestionsService) {
    }

    @Post()
    @Roles('rkn')
    create(@Body() createFaqQuestionDto: CreateFaqQuestionDto) {
        return this.faqQuestionsService.create(createFaqQuestionDto);
    }

    @Get()
    @Roles('rkn')
    findAll() {
        return this.faqQuestionsService.findAll();
    }

    @Get(':id')
    @Roles('rkn')
    findOne(@Param('id') id: string) {
        return this.faqQuestionsService.findOne(+id);
    }

    @Patch(':id')
    @Roles('rkn')
    update(@Param('id') id: string, @Body() updateFaqQuestionDto: UpdateFaqQuestionDto) {
        return this.faqQuestionsService.update(+id, updateFaqQuestionDto);
    }

    @Delete(':id')
    @Roles('rkn')
    remove(@Param('id') id: string) {
        return this.faqQuestionsService.remove(+id);
    }
}

import { Injectable } from "@nestjs/common";
import { CreateFaqQuestionDto } from "./dto/create-faq-question.dto";
import { UpdateFaqQuestionDto } from "./dto/update-faq-question.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { FaqQuestion } from "./faq-question.entity";
import { Repository } from "typeorm";

@Injectable()
export class FaqQuestionsService {
    constructor(@InjectRepository(FaqQuestion) private faqQuestionsRepository: Repository<FaqQuestion>) {
    }

    create(createFaqQuestionDto: CreateFaqQuestionDto) {
        return 'This action adds a new faqQuestion';
    }

    findAll() {
        return `This action returns all faqQuestions`;
    }

    findOne(id: number) {
        return `This action returns a #${id} faqQuestion`;
    }

    update(id: number, updateFaqQuestionDto: UpdateFaqQuestionDto) {
        return `This action updates a #${id} faqQuestion`;
    }

    remove(id: number) {
        return `This action removes a #${id} faqQuestion`;
    }
}

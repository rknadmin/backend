import { Column, Entity, ManyToOne } from "typeorm";
import { TimeStampedEntity } from "../../common/entities/time-stamped.entity";
import { FaqCategory } from "../faq-categories/faq-category.entity";
import { IsNotEmpty } from "class-validator";

@Entity()
export class FaqQuestion extends TimeStampedEntity {
    @Column()
    @IsNotEmpty()
    name: string;

    @Column('longtext')
    @IsNotEmpty()
    answer: string;

    @ManyToOne((type) => FaqCategory, (category) => category.questions, {onDelete: 'SET NULL'})
    category: FaqCategory;
}

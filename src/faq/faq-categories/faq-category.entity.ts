import { Column, Entity, OneToMany } from "typeorm";
import { TimeStampedEntity } from "../../common/entities/time-stamped.entity";
import { FaqQuestion } from "../faq-questions/faq-question.entity";
import { IsNotEmpty } from "class-validator";

@Entity()
export class FaqCategory extends TimeStampedEntity {
    @Column()
    @IsNotEmpty()
    name: string;

    @Column('longtext', {nullable: true})
    description: string;

    @OneToMany((type) => FaqQuestion, (question) => question.category, {eager: true})
    questions: FaqQuestion[];
}

import { Module } from "@nestjs/common";
import { FaqCategoriesService } from "./faq-categories.service";
import { FaqCategoriesController } from "./faq-categories.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { FaqCategory } from "./faq-category.entity";

@Module({
    imports: [TypeOrmModule.forFeature([FaqCategory])],
    controllers: [FaqCategoriesController],
    providers: [FaqCategoriesService],
    exports: [FaqCategoriesService],
})
export class FaqCategoriesModule {
}

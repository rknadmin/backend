import { Body, Controller, Delete, Get, Param, Patch, Post, UseFilters, UseGuards } from "@nestjs/common";
import { FaqCategoriesService } from "./faq-categories.service";
import { CreateFaqCategoryDto } from "./dto/create-faq-category.dto";
import { UpdateFaqCategoryDto } from "./dto/update-faq-category.dto";
import { AuthExceptionFilter } from "../../common/filters/auth-exceptions.filter";
import { AuthenticatedGuard } from "../../common/guards/authenticated.guard";
import { Roles } from "../../common/decorators/roles.decorator";

@Controller('api/faq-categories')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class FaqCategoriesController {
    constructor(private readonly faqCategoriesService: FaqCategoriesService) {
    }

    @Post()
    @Roles('rkn')
    create(@Body() createFaqCategoryDto: CreateFaqCategoryDto) {
        return this.faqCategoriesService.create(createFaqCategoryDto);
    }

    @Get()
    @Roles('rkn')
    async findAll() {
        return await this.faqCategoriesService.findAll();
    }

    @Get(':id')
    @Roles('rkn')
    findOne(@Param('id') id: string) {
        return this.faqCategoriesService.findOne(+id);
    }

    @Patch(':id')
    @Roles('rkn')
    update(@Param('id') id: string, @Body() updateFaqCategoryDto: UpdateFaqCategoryDto) {
        return this.faqCategoriesService.update(+id, updateFaqCategoryDto);
    }

    @Delete(':id')
    @Roles('rkn')
    remove(@Param('id') id: string) {
        return this.faqCategoriesService.remove(+id);
    }
}

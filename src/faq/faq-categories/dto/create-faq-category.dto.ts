import { IsNotEmpty } from "class-validator";

export class CreateFaqCategoryDto {
    @IsNotEmpty()
    name!: string;

    description?: string
}

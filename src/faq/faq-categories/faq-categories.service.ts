import { Injectable } from "@nestjs/common";
import { CreateFaqCategoryDto } from "./dto/create-faq-category.dto";
import { UpdateFaqCategoryDto } from "./dto/update-faq-category.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { FaqCategory } from "./faq-category.entity";
import { Repository } from "typeorm";

@Injectable()
export class FaqCategoriesService {
    constructor(@InjectRepository(FaqCategory) private faqCategoriesRepository: Repository<FaqCategory>) {
    }

    create(createFaqCategoryDto: CreateFaqCategoryDto) {
        return 'This action adds a new faqCategory';
    }

    async findAll() {
        // return await this.faqCategoriesRepository.find({relations: ['questions']});
        return await this.faqCategoriesRepository.createQueryBuilder('categories')
            .leftJoinAndSelect('categories.questions', 'question')
            .orderBy('categories.name')
            .addOrderBy('question.name')
            .getMany();
    }

    findOne(id: number) {
        return `This action returns a #${id} faqCategory`;
    }

    update(id: number, updateFaqCategoryDto: UpdateFaqCategoryDto) {
        return `This action updates a #${id} faqCategory`;
    }

    remove(id: number) {
        return `This action removes a #${id} faqCategory`;
    }
}

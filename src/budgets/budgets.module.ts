import { Module } from '@nestjs/common';
import { BudgetsService } from './budgets.service';
import { BudgetsController } from './budgets.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Budget } from './budget.entity';

@Module({
    imports: [TypeOrmModule.forFeature([Budget])],
    controllers: [BudgetsController],
    providers: [BudgetsService],
    exports: [BudgetsService]
})
export class BudgetsModule {
}

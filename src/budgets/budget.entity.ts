import { Column, Entity, JoinColumn, ManyToOne, OneToMany } from 'typeorm';
import { TimeStampedEntity } from '../common/entities/time-stamped.entity';
import { Account } from '../accounts/account.entity';
import { Society } from '../societies/society.entity';
import { Funding } from '../fundings/funding.entity';
import { Grant } from '../grants/grant.entity';
import { ColumnNumericTransformer } from '../common/transformers/column-numeric.transformer';

@Entity()
export class Budget extends TimeStampedEntity {
    @Column({type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer(),})
    total: number;

    @Column({type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer(),})
    spent: number;

    @Column({type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer(),})
    requested: number;

    @Column({type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer(),})
    granted: number;

    @Column({type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer(),})
    left: number;

    @ManyToOne((type) => Account, (account) => account.budgets, {onDelete: 'CASCADE'})
    @JoinColumn({name: 'accountId'})
    account: Account;

    @Column()
    accountId: number;

    @ManyToOne((type) => Society, (society) => society.budgets, {onDelete: 'CASCADE'})
    @JoinColumn({name: 'societyId'})
    society: Society;

    @Column()
    societyId: number;

    @OneToMany((type) => Funding, (funding) => funding.budget, {eager: true})
    fundings: Funding[];

    @OneToMany((type) => Grant, (grant) => grant.budget, {eager: true})
    grants: Grant[];
}

import {Inject, Injectable} from "@nestjs/common";
import {InjectRepository} from "@nestjs/typeorm";
import {Budget} from "./budget.entity";
import {Repository} from "typeorm";
import {WINSTON_MODULE_NEST_PROVIDER} from "nest-winston";

@Injectable()
export class BudgetsService {
    constructor(
        @InjectRepository(Budget) private budgetsRepository: Repository<Budget>,
        @Inject(WINSTON_MODULE_NEST_PROVIDER) private readonly logger
    ) {
        this.logger.setContext(BudgetsService.name)
    }

    /**
     * Creates a budget for a given account and society.
     * @param accountId - The account id.
     * @param societyId - The society id.
     * @returns The created budget.
     */
    async create(accountId: number, societyId: number): Promise<Budget> {
        this.logger.debug(`Creating budget for account ID ${accountId} and society ID ${societyId}.`);
        return await this.budgetsRepository.save({
            accountId,
            societyId
        });
    }

    /**
     * Retrieves one budget by its id.
     * @param id - The budget id.
     * @returns The budget.
     */
    async findOne(id: number): Promise<Budget> {
        return await this.budgetsRepository.createQueryBuilder('budget')
            .where('budget.id = :id', {id})
            .leftJoinAndSelect('budget.account', 'account')
            .getOne();
    }

    /**
     * Retrieves all budgets.
     * @param id - The budget id.
     * @param updateBudgetDto - The budget data to update.
     * @returns The updated budget.
     */
    async update(id: number, updateBudgetDto): Promise<Budget> {
        const budget = await this.findOne(id);

        if (updateBudgetDto['total']) {
            const difference = budget.total - updateBudgetDto.total;
            updateBudgetDto['total'] = budget.total - difference;
        }

        this.logger.debug(`Updating budget ID ${id} with data ${JSON.stringify(updateBudgetDto)}.`);
        return await this.budgetsRepository.save({id, ...updateBudgetDto});
    }

    /**
     * Deletes a budget.
     * @param data - The budget data to delete.
     * @returns The number of affected rows.
     */
    async remove(data: { societyId: string, accountId: string }) {
        const budget = await this.budgetsRepository.createQueryBuilder('budget')
            .where('budget.accountId = :accountId', {accountId: +data.accountId})
            .andWhere('budget.societyId = :societyId', {societyId: +data.societyId})
            .getOne();

        this.logger.debug(`Deleting budget for account ID ${data.accountId} and society ID ${data.societyId}.`);
        return await this.budgetsRepository.remove(budget);
    }

    /**
     * Retrieves a single budget of a given account and society.
     * @param accountId - The account id.
     * @param societyId - The society id.
     * @returns The budget.
     */
    async findWithinAccount(accountId: number, societyId: number) {
        this.logger.debug(`Retrieving budget for account ID ${accountId} and society ID ${societyId}.`);
        return await this.budgetsRepository.createQueryBuilder('budget')
            .where('budget.accountId = :accountId', {accountId})
            .andWhere('budget.societyId = :societyId', {societyId})
            .getOne();
    }
}

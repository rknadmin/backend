import {Body, Controller, Delete, Get, Param, Put, UseFilters, UseGuards} from '@nestjs/common';
import {BudgetsService} from './budgets.service';
import {UpdateBudgetDto} from './dto/update-budget.dto';
import {AuthExceptionFilter} from '../common/filters/auth-exceptions.filter';
import {AuthenticatedGuard} from '../common/guards/authenticated.guard';
import {Roles} from '../common/decorators/roles.decorator';

@Controller('api/budgets')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class BudgetsController {
    constructor(private readonly budgetsService: BudgetsService) {
    }

    @Get(':id')
    @Roles('zco')
    async findOne(@Param('id') id: string) {
        return await this.budgetsService.findOne(+id);
    }

    @Put(':id')
    @Roles('zco')
    async update(@Param('id') id: string, @Body() updateBudgetDto: UpdateBudgetDto) {
        return await this.budgetsService.update(+id, updateBudgetDto);
    }

    @Delete()
    @Roles('zco')
    async remove(@Body() data: { societyId: string, accountId: string }) {
        return await this.budgetsService.remove(data);
    }
}

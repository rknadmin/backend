import {BeforeInsert, BeforeUpdate, Column, Entity, JoinTable, ManyToMany, OneToMany} from 'typeorm';
import {TimeStampedEntity} from '../common/entities/time-stamped.entity';
import {IsEmail, IsNotEmpty, IsPositive} from 'class-validator';
import {IsPutDomain} from '../common/decorators/email-domain.decorator';
import UserRoleType from './enums/user-role-type.enum';
import {Society} from "../societies/society.entity";
import {Grant} from "../grants/grant.entity";
import {Funding} from "../fundings/funding.entity";

@Entity()
export class User extends TimeStampedEntity {
    @Column({update: false, unique: true})
    @IsNotEmpty()
    @IsEmail()
    @IsPutDomain()
    email: string;

    @Column({default: false})
    emailVerified: boolean;

    @Column()
    name: string;

    @Column()
    @IsNotEmpty()
    @IsPositive()
    index: number;

    @Column({default: UserRoleType.User})
    role: UserRoleType;

    @Column({default: true})
    active: boolean;

    @Column({default: false})
    verified: boolean;

    @ManyToMany(() => Society)
    @JoinTable()
    societies: Society[];

    @OneToMany((type) => Grant, (grant) => grant.user, {eager: true})
    grants: Grant[];

    @OneToMany((type) => Funding, (funding) => funding.user, {eager: true})
    fundings: Funding[];

    @Column({nullable: true})
    lastLogin: Date;
}

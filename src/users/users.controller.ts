import {
    Body,
    Controller,
    Delete,
    Get,
    Param,
    Patch,
    Post,
    Put,
    Res,
    UseFilters,
    UseGuards
} from '@nestjs/common';
import {UsersService} from './users.service';
import {CreateUserDto} from './dto/create-user.dto';
import {UpdateUserDto} from './dto/update-user.dto';
import {AuthExceptionFilter} from "../common/filters/auth-exceptions.filter";
import {AuthenticatedGuard} from "../common/guards/authenticated.guard";
import {Roles} from "../common/decorators/roles.decorator";

@Controller('api/users')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class UsersController {
    constructor(private readonly usersService: UsersService) {
    }

    @Post()
    @Roles('rkn', 'zco')
    async create(@Body() createUserDto: CreateUserDto) {
        return await this.usersService.create(createUserDto);
    }

    @Get()
    @Roles('rkn', 'zco')
    async findAll() {
        return await this.usersService.findAll();
    }

    @Get(':id')
    @Roles('rkn', 'zco')
    async findOne(@Param('id') id: string) {
        return await this.usersService.findOne(+id);
    }

    @Put(':id')
    @Roles('rkn', 'zco')
    async update(@Param('id') id: string, @Body() updateUserDto: UpdateUserDto) {
        return await this.usersService.update(+id, updateUserDto);
    }

    @Patch('verify/:id')
    @Roles('rkn', 'zco')
    async verifyUser(@Param('id') id: string) {
        return await this.usersService.verify(+id);
    }

    @Patch('verify-email/:id')
    @Roles('rkn', 'zco')
    async verifyUserEmail(@Param('id') id: string) {
        return await this.usersService.verifyEmail(+id);
    }

    @Patch('activate/:id')
    @Roles('rkn', 'zco')
    async activateUser(@Param('id') id: string) {
        return await this.usersService.activate(+id);
    }

    @Patch('suspend/:id')
    @Roles('rkn', 'zco')
    async suspendUser(@Param('id') id: string) {
        return await this.usersService.suspend(+id);
    }

    @Delete(':userId/societies/:societyId')
    @Roles('rkn', 'zco')
    async removeRelation(@Param('userId') id: string, @Param('societyId') societyId: string) {
        return await this.usersService.removeRelation(+id, +societyId);
    }

    @Delete(':id')
    @Roles('rkn', 'zco')
    async remove(@Res() res, @Param('id') id: string) {
        await this.usersService.remove(+id);
        return res.status(204).send();
    }
}

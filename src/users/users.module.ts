import {Module} from '@nestjs/common';
import {UsersService} from './users.service';
import {UsersController} from './users.controller';
import {TypeOrmModule} from '@nestjs/typeorm';
import {User} from './user.entity';
import {SocietiesModule} from '../societies/societies.module';
import {LoginVerification} from "../auth/login-verification.entity";

@Module({
    imports: [TypeOrmModule.forFeature([User]), TypeOrmModule.forFeature([LoginVerification]), SocietiesModule],
    controllers: [UsersController],
    providers: [UsersService],
    exports: [UsersService]
})
export class UsersModule {
}

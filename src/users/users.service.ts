import {Injectable, InternalServerErrorException} from '@nestjs/common';
import {CreateUserDto} from './dto/create-user.dto';
import {UpdateUserDto} from './dto/update-user.dto';
import {InjectRepository} from '@nestjs/typeorm';
import {User} from './user.entity';
import {Repository} from 'typeorm';
import {SocietiesService} from '../societies/societies.service';
import {RegisterUserDto} from "./dto/register-user.dto";
import {MailerService} from "@nestjs-modules/mailer";
import {LoginVerification} from "../auth/login-verification.entity";

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User) private usersRepository: Repository<User>,
        @InjectRepository(LoginVerification) private loginVerificationRepository: Repository<LoginVerification>,
        private societiesService: SocietiesService,
        private readonly mailerService: MailerService
    ) {
    }

    async findUser(email: string): Promise<any> {
        return await this.usersRepository.createQueryBuilder('user')
            .where('user.email = :email', {email})
            .leftJoinAndSelect('user.societies', 'society')
            .getOne();
    }

    async register(registerUserDto: RegisterUserDto) {
        const {societyId, ...data} = registerUserDto;
        const user = this.usersRepository.create(data);
        const society = await this.societiesService.findOne(+societyId);
        user.societies = [society];
        return await this.saveHelper(user);
    }

    async create(createUserDto: CreateUserDto) {
        const {societyId, ...data} = createUserDto;
        const user = this.usersRepository.create(data);

        user.emailVerified = false;
        user.verified = false;

        user.societies = [];
        if (societyId) {
            for (const id of societyId) {
                const society = await this.societiesService.findOne(+id);
                user.societies.push(society);
            }
        }

        await this.saveHelper(user);
    }

    async findAll() {
        return await this.usersRepository.createQueryBuilder('user')
            .leftJoinAndSelect('user.societies', 'society')
            .getMany();
        // return await this.usersRepository.find({relations: ['societies']});
    }

    async findOne(id: number) {
        return await this.usersRepository.createQueryBuilder('user')
            .where('user.id = :id', {id})
            .leftJoinAndSelect('user.societies', 'society')
            .getOne();
    }

    async getThis() {
        return await this.usersRepository.createQueryBuilder('user')
            .where('user.id = :id', {id: 1})
            .leftJoinAndSelect('user.societies', 'society')
            .orderBy('society.name')
            .getOne();
    }

    async update(id: number, updateUserDto: UpdateUserDto) {
        const user = await this.findOne(id);
        const {societyId, ...data} = updateUserDto;
        for (const prop in data) user[prop] = data[prop];
        user.societies = [];
        if (societyId) {
            for (const id of societyId) {
                const society = await this.societiesService.findOne(+id);
                user.societies.push(society);
            }
        }
        return await this.saveHelper(user);
    }

    async verify(id: number) {
        const user = await this.findOne(id);
        user.verified = true;
        await this.saveHelper(user).catch(err => { throw new InternalServerErrorException(err) });

        await this.mailerService.sendMail({
            to: user.email,
            from: 'RKNAdmin <donotreply@rkn.put.poznan.pl>',
            subject: '[RKNAdmin] Weryfikacja konta',
            template: 'base',
            context: {
                header: 'Weryfikacja konta',
                message: 'Potwierdzamy weryfikację konta w systemie RKNAdmin. Możesz zalogować się korzystając z poniższego linku.',
                url: 'https://rkn.put.poznan.pl/admin/auth/login'
            }
        }).then(() => {
            return user;
        }).catch(() => {
            throw new InternalServerErrorException('Wystąpił błąd podczas wysyłania wiadomości e-mail');
        });
    }

    async verifyEmail(id: number) {
        const user = await this.findOne(id);
        user.emailVerified = true;
        await this.saveHelper(user).catch(err => { throw new InternalServerErrorException(err) });

        await this.mailerService.sendMail({
            to: user.email,
            from: 'RKNAdmin <donotreply@rkn.put.poznan.pl>',
            subject: '[RKNAdmin] Weryfikacja adresu e-mail',
            template: 'base',
            context: {
                header: 'Weryfikacja adresu e-mail',
                message: 'Twój adres e-mail został ręcznie zweryfikowany.',
                url: 'https://rkn.put.poznan.pl/admin/auth/login'
            }
        }).then(() => {
            return user;
        }).catch((err) => {
            console.log(err);
            throw new InternalServerErrorException('Wystąpił błąd podczas wysyłania wiadomości e-mail');
        });
    }


    async activate(id: number) {
        const user = await this.findOne(id);
        user.active = true;
        await this.saveHelper(user).catch(err => { throw new InternalServerErrorException(err) });

        await this.mailerService.sendMail({
            to: user.email,
            from: 'RKNAdmin <donotreply@rkn.put.poznan.pl>',
            subject: '[RKNAdmin] Aktywacja konta',
            template: 'base',
            context: {
                header: 'Aktywacja konta',
                message: 'Twoje konto w systemie RKNAdmin zostało ponownie aktywowane. Możesz zalogować się korzystając z poniższego linku.',
                url: 'https://rkn.put.poznan.pl/admin/auth/login'
            }
        }).then(() => {
            return user;
        }).catch(() => {
            throw new InternalServerErrorException('Wystąpił błąd podczas wysyłania wiadomości e-mail');
        });
    }

    async suspend(id: number) {
        const user = await this.findOne(id);

        user.active = false;
        await this.saveHelper(user).catch(err => { throw new InternalServerErrorException(err) });

        await this.mailerService.sendMail({
            to: user.email,
            from: 'RKNAdmin <donotreply@rkn.put.poznan.pl>',
            subject: '[RKNAdmin] Zawieszenie konta',
            template: 'base',
            context: {
                header: 'Zawieszenie konta',
                message: 'Twoje konto w systemie RKNAdmin zostało zawieszone.',
            }
        }).then(() => {
            return user;
        }).catch(() => {
            throw new InternalServerErrorException('Wystąpił błąd podczas wysyłania wiadomości e-mail');
        });
    }


    async remove(id: number) {
        const user = await this.findOne(id);
        const loginVerification = await this.loginVerificationRepository.createQueryBuilder('loginVerification')
            .where('email = :email', { email: user.email })
            .getOne();
        if (loginVerification) await this.loginVerificationRepository.delete(loginVerification.id);
        const result = await this.usersRepository.delete(id);

        if (result) {
            return user;
        } throw new InternalServerErrorException('Wystąpił błąd podczas usuwania użytkownika');
    }

    async removeRelation(id: number, societyId: number) {
        const user = await this.usersRepository.createQueryBuilder('user')
            .where('user.id = :id', {id})
            .leftJoinAndSelect('user.societies', 'society')
            .getOne();
        user.societies = user.societies.filter(society => society.id !== societyId);
        return await user.save();
    }

    async saveHelper(user: User) {
        try {
            return await user.save();
        } catch (err) {
            if (err.errno === 1062)
                throw new InternalServerErrorException(
                    `User with email "${user.email}" already exists.`,
                );
            else throw new InternalServerErrorException();
        }
    }
}

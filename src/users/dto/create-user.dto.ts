import { IsEmail, IsNotEmpty } from "class-validator";
import { IsPutDomain } from "../../common/decorators/email-domain.decorator";
import UserRoleType from "../enums/user-role-type.enum";
import { ToBoolean } from "../../common/decorators/to-boolean";

export class CreateUserDto {
    @IsNotEmpty()
    societyId!: string[];

    @IsNotEmpty()
    @IsEmail()
    @IsPutDomain()
    email!: string;

    @IsNotEmpty()
    index!: number;

    role?: UserRoleType;

    @ToBoolean()
    active?: boolean;
}

import UserRoleType from "../enums/user-role-type.enum";
import { ToBoolean } from "../../common/decorators/to-boolean";

export class UpdateUserDto {
    societyId?: number[];
    name?: string;
    email?: string;
    index?: number;
    role?: UserRoleType;

    @ToBoolean()
    active?: boolean;

    @ToBoolean()
    verified?: boolean;
}

import { IsEmail, IsPositive } from "class-validator";
import { IsPutDomain } from "../../common/decorators/email-domain.decorator";
import UserRoleType from "../enums/user-role-type.enum";
import { ToBoolean } from "../../common/decorators/to-boolean";

export class PatchUserDto {
    @IsEmail()
    @IsPutDomain()
    email?: string;

    @IsPositive()
    index?: number;

    name?: string;
    role?: UserRoleType;

    @ToBoolean()
    active?: boolean;
}

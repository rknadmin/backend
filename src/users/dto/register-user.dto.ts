import {IsEmail, IsNotEmpty} from "class-validator";
import {IsPutDomain} from "../../common/decorators/email-domain.decorator";

export class RegisterUserDto {
    @IsNotEmpty()
    societyId!: number;

    @IsNotEmpty()
    name!: string;

    @IsNotEmpty()
    @IsEmail()
    @IsPutDomain()
    email!: string;

    @IsNotEmpty()
    index!: number;
}

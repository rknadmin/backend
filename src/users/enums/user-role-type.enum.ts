enum UserRoleType {
    Root = 'root',
    Rkn = 'rkn',
    Zco = 'zco',
    Rector = 'rector',
    User = 'user',
}

export default UserRoleType;
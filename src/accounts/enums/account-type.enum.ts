enum AccountType {
    Statutory = 'statutory',
    Additional = 'additional',
    Sponsorship = 'sponsorship',
    Other = 'other',
    Grant = 'grant'
}

export default AccountType;
import { Column, Entity, OneToMany } from 'typeorm';
import { TimeStampedEntity } from '../common/entities/time-stamped.entity';
import { Budget } from '../budgets/budget.entity';
import AccountType from './enums/account-type.enum';
import { ColumnNumericTransformer } from '../common/transformers/column-numeric.transformer';

@Entity()
export class Account extends TimeStampedEntity {
    @Column({ unique: true })
    name: string;

    @Column({ type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer() })
    total: number;

    @Column({ type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer() })
    spent: number;

    @Column({ type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer() })
    requested: number;

    @Column({ type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer() })
    granted: number;

    @Column({ type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer() })
    left: number;

    @Column({ default: false })
    active: boolean;

    @Column()
    type: AccountType;

    @Column({ default: false })
    unlimited: boolean;

    @Column({default: true})
    amountVisible: boolean;

    @OneToMany((type) => Budget, (budget) => budget.account, {eager: true})
    budgets: Budget[];
}

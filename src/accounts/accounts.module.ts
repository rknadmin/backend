import { Module } from '@nestjs/common';
import { AccountsService } from './accounts.service';
import { AccountsController } from './accounts.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Account } from './account.entity';
import { BudgetsModule } from '../budgets/budgets.module';

@Module({
    imports: [TypeOrmModule.forFeature([Account]), BudgetsModule],
    controllers: [AccountsController],
    providers: [AccountsService],
    exports: [AccountsService]
})
export class AccountsModule {
}

import { Body, Controller, Delete, Get, Param, Post, Put, UseFilters, UseGuards } from '@nestjs/common';
import { AccountsService } from './accounts.service';
import { CreateAccountDto } from './dto/create-account.dto';
import { UpdateAccountDto } from './dto/update-account.dto';
import { AuthExceptionFilter } from '../common/filters/auth-exceptions.filter';
import { AuthenticatedGuard } from '../common/guards/authenticated.guard';
import { Roles } from '../common/decorators/roles.decorator';


@Controller('api/accounts')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class AccountsController {
    constructor(private readonly accountsService: AccountsService) {
    }

    @Post()
    @Roles('zco')
    async create(@Body() createAccountDto: CreateAccountDto) {
        return await this.accountsService.create(createAccountDto);
    }

    @Post(':id/distribute')
    @Roles('zco')
    async distribute(@Param('id') id: string, @Body() budgets) {
        return await this.accountsService.distribute(+id, budgets);
    }

    @Get()
    @Roles('zco')
    findAll() {
        return this.accountsService.findAll();
    }

    @Get(':id')
    @Roles('zco')
    findOne(@Param('id') id: string) {
        return this.accountsService.findOne(+id);
    }

    @Put(':id')
    @Roles('zco')
    async update(@Param('id') id: string, @Body() updateAccountDto: UpdateAccountDto) {
        return await this.accountsService.update(+id, updateAccountDto);
    }

    @Delete(':id')
    @Roles('zco')
    async remove(@Param('id') id: string) {
        return await this.accountsService.remove(+id);
    }
}

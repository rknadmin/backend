import { ToBoolean } from '../../common/decorators/to-boolean';
import AccountType from '../enums/account-type.enum';

export class CreateAccountDto {
    societyId?: string[];

    name!: string;
    total?: number;
    type!: AccountType;

    @ToBoolean()
    active!: boolean;

    @ToBoolean()
    unlimited!: boolean;

    @ToBoolean()
    amountVisible!: boolean;
}

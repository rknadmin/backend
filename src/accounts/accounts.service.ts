import {Inject, Injectable, InternalServerErrorException} from '@nestjs/common';
import {CreateAccountDto} from './dto/create-account.dto';
import {UpdateAccountDto} from './dto/update-account.dto';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {Account} from './account.entity';
import {BudgetsService} from '../budgets/budgets.service';
import {WINSTON_MODULE_NEST_PROVIDER} from 'nest-winston';
import {DeleteResult} from 'typeorm/query-builder/result/DeleteResult';

@Injectable()
export class AccountsService {
    constructor(
        @InjectRepository(Account) private accountsRepository: Repository<Account>,
        private budgetsService: BudgetsService,
        @Inject(WINSTON_MODULE_NEST_PROVIDER) private readonly logger
    ) {
        this.logger.setContext(AccountsService.name)
    }

    /**
     * Creates a new account.
     * @param createAccountDto - new account data.
     * @returns newly created account saved in database.
     */

    async create(createAccountDto: CreateAccountDto): Promise<Account> {
        const {societyId, ...data} = createAccountDto;

        data.amountVisible = (!data.unlimited && data.amountVisible);
        data.total = (data.unlimited) ? 0 : data.total;

        const account = this.accountsRepository.create({
            ...data,
            left: data.total
        });
        account.budgets = [];
        const savedAccount = await account.save();

        if (societyId) {
            for (const id of societyId) {
                const budget = await this.budgetsService.create(savedAccount.id, +id);//, 0);
                savedAccount.budgets.push(budget);
            }
        }

        this.logger.log(`Account created with ID: ${savedAccount.id}, total budgets: ${societyId.length}.`)
        return await account.save();
    }

    /**
     * Updates account with specific id.
     * @param id - ID of the account to update.
     * @param updateAccountDto - new account data.
     * @returns updated account saved in database.
     */
    async update(id: number, updateAccountDto: UpdateAccountDto): Promise<Account> {
        const {societyId, ...data} = updateAccountDto;
        const account = await this.accountsRepository.findOneBy({id});

        if (updateAccountDto['total']) {
            const diff = account.total - updateAccountDto.total;
            account.total -= diff;
        }

        data.amountVisible = (!data.unlimited && data.amountVisible);
        for (const prop in data) account[prop] = data[prop];

        if (societyId) {
            for (const id of societyId) {
                if (!account.budgets.some(b => b.societyId === +id)) {
                    const budget = await this.budgetsService.create(account.id, +id);
                    account.budgets.push(budget);
                }
            }
        }

        this.logger.log(`Account ${id} updated.`)
        return await account.save();
    }

    /**
     * Distributes account's total amount across its budgets (societies).
     * @param id - ID of the account to distribute.
     * @param budgets - budgets (societies) to distribute across.
     */
    async distribute(id: number, budgets): Promise<void> {
        const account = await this.findOne(id);

        Object.keys(budgets).forEach((key) => {
            budgets[key] = parseFloat(budgets[key])
        });

        const sum = Object.values(budgets).reduce((a: number, b: number) => a + b);

        if (+sum <= account.total) {
            for (const [key, val] of Object.entries(budgets)) {
                const budget = await this.budgetsService.findWithinAccount(id, +key);
                budget.total = +val;
                budget.left = budget.total;
                await budget.save();
                this.logger.log(`Account ID: ${id} distributed across ${Object.keys(budgets).length} budgets (societies).`)
            }
        } else {
            throw new InternalServerErrorException(`Distributed amount exceeds the total account's amount!`);
        }
    }

    /**
     * Retrieves all accounts (with their relations) from the database.
     * @returns all accounts saved in database.
     */
    async findAll(): Promise<Account[]> {
        return await this.accountsRepository.createQueryBuilder('account')
            .leftJoinAndSelect('account.budgets', 'budget')
            .orderBy('account.createdAt', 'DESC')
            .addOrderBy('budget.createdAt', 'DESC')
            .getMany();
    }

    /**
     * Retrieves account with specific id (with its relations) from the database.
     * @param id - ID of the account to retrieve.
     * @returns account with specific id.
     */
    async findOne(id: number): Promise<Account> {
        return await this.accountsRepository.createQueryBuilder('account')
            .where('account.id = :id', {id})
            .leftJoinAndSelect('account.budgets', 'budget')
            .leftJoinAndSelect('budget.society', 'society')
            .leftJoinAndSelect('budget.account', 'budgetAccount')
            .leftJoinAndSelect('budget.grants', 'grant')
            .leftJoinAndSelect('budget.fundings', 'funding')
            .leftJoinAndSelect('society.budgets', 'societyBudget')
            .getOne();
    }

    /**
     * Removes account with specific id from the database.
     * @param id - ID of the account to remove.
     * @returns number of affected rows.
     */
    async remove(id: number): Promise<DeleteResult> {
        this.logger.log(`Account ID: ${id} removed.`)
        return await this.accountsRepository.delete(id);
    }
}

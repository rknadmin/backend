import { Injectable, InternalServerErrorException } from "@nestjs/common";
import { CreateGrantDto } from "./dto/create-grant.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { SocietiesService } from "../societies/societies.service";
import { Grant } from "./grant.entity";
import { Repository } from "typeorm";
import { User } from "../users/user.entity";
import { BudgetsService } from "../budgets/budgets.service";
import AccountType from "../accounts/enums/account-type.enum";
import GrantStatus from "./enums/grant-status.enum";
import EntityType from "../files/enums/entity-type.enum";
import { FilesService } from "../files/files.service";
import { MailerService } from "@nestjs-modules/mailer";
import moment = require("moment");


@Injectable()
export class GrantsService {
  constructor(
    @InjectRepository(Grant) private grantsRepository: Repository<Grant>,
    private societiesService: SocietiesService,
    private budgetsService: BudgetsService,
    private filesService: FilesService,
    private readonly mailerService: MailerService
  ) {
    }

  async create(createGrantDto: CreateGrantDto, user: User, files: Array<Express.Multer.File>) {
    const { societyId, budgetId, ...data } = createGrantDto;

    const society = await this.societiesService.findOne(+societyId);
    const budget = await this.budgetsService.findOne(+budgetId);

    if ((user.societies.some(s => s.id === +societyId) && society.budgets.some(b => b.id === +budgetId)) || ["zco", "rkn"].includes(user.role)) {
      if (budget.account.type === AccountType.Grant && (budget.account.unlimited || !budget.account.amountVisible || data.requested <= budget.left)) {
        const grant = this.grantsRepository.create({
          ...data,
          user: user,
          granted: 0,
          status: GrantStatus.Submitted,
          budget,
          society
        });

                if (!data['email'] || data['email'] === '') grant['email'] = user.email;
                if (!data['applicant'] || data['applicant'] === '') grant['applicant'] = user.name;

                try {
                  const savedGrant = await grant.save();

                  budget.requested += +data.requested;
                  budget.left -= +data.requested;
                  await budget.save();

                  budget.account.requested += +data.requested;
                  budget.account.left -= +data.requested;
                  await budget.account.save();

                  for (const file of files)
                    await this.filesService.create(file, EntityType.Grant, savedGrant.id);

                  return savedGrant;
                } catch (e) {
                    throw new InternalServerErrorException(e);
                }
            } else throw new InternalServerErrorException('Przekroczono budżet!');
        } else throw new InternalServerErrorException('Brak uprawnień!');
    }

  async findAll() {
    return await this.grantsRepository.createQueryBuilder("grant")
      .leftJoinAndSelect("grant.budget", "budget")
      .leftJoinAndSelect("budget.account", "account")
      .leftJoinAndSelect("grant.society", "society")
      .getMany();
  }

  async findOne(id: number) {
    return await this.grantsRepository.createQueryBuilder("grant")
      .where("grant.id = :id", { id })
      .leftJoinAndSelect("grant.user", "user")
      .leftJoinAndSelect("grant.budget", "budget")
      .leftJoinAndSelect("budget.account", "account")
      .leftJoinAndSelect("grant.society", "society")
      .leftJoinAndSelect("society.faculty", "faculty")
      .getOne();
  }

  async updateStatus(id: number, status: GrantStatus, data) {
    const grant = await this.findOne(id);

    grant.status = status;

    if (status === GrantStatus.Appointment) {

      if (data.appointmentDate) {
        const date = moment(data.appointmentDate).format("DD.MM [o godz.] HH:mm");

        await this.mailerService.sendMail({
          to: grant.email,
          from: "RKNAdmin <donotreply@rkn.put.poznan.pl>",
          subject: "[RKNAdmin] Dostarcz wydrukowany wniosek",
          template: "base",
          context: {
            header: "Wniosek zaakceptowany przez ZCO",
            message: `Twój wniosek: ${grant.subject} został zaakceptowany przez ZCO. Wydrukuj wniosek i przynieś do ZCO (pamiętaj o wymaganych podpisach), proponowany termin dostarczenia to: ${date}. W celu zmiany terminu skontakuj się z Iwoną Michałowską (iwona.michalowska@put.poznan.pl).`
          }
        }).then(() => {
          return;
        }).catch((e) => {
          throw new InternalServerErrorException("Wystąpił błąd podczas wysyłania wiadomości e-mail");
        });
      }

    } else if (status === GrantStatus.Pending) {

      await this.mailerService.sendMail({
        to: grant.email,
        from: "RKNAdmin <donotreply@rkn.put.poznan.pl>",
        subject: "[RKNAdmin] Wniosek zaakceptowany",
        template: "base",
        context: {
          header: "Wniosek zaakceptowany",
          message: `Twój wniosek: ${grant.subject} został zaakceptowany. Możesz rozpocząć jego rozliczanie.`
        }
      }).then(() => {
        return;
      }).catch((e) => {
        throw new InternalServerErrorException("Wystąpił błąd podczas wysyłania wiadomości e-mail");
      });

      grant.budget.granted += +data.granted;
      grant.budget.left += +grant.requested;
      grant.budget.left -= +data.granted;
      await grant.budget.save();

      grant.budget.account.granted += +data.granted;
      grant.budget.account.left += +grant.requested;
      grant.budget.account.left -= +data.granted;
      await grant.budget.account.save();

      grant.granted = +data.granted;
      return await grant.save();

    } else if (status === GrantStatus.Completed) {

      await this.mailerService.sendMail({
        to: grant.email,
        from: "RKNAdmin <donotreply@rkn.put.poznan.pl>",
        subject: "[RKNAdmin] Rozliczanie wniosku zakończone",
        template: "base",
        context: {
          header: "Rozliczanie wniosku zakończone",
          message: `Rozliczanie Twojego wniosku: ${grant.subject} zostało zakończone. Niewydana kwota została ponownie włączona do dostępnego budżetu Twojej organizacji.`
        }
      }).then(() => {
        return;
      }).catch((e) => {
        throw new InternalServerErrorException("Wystąpił błąd podczas wysyłania wiadomości e-mail");
      });

      grant.budget.spent += +grant.spent;
      grant.budget.left += +grant.granted;
      grant.budget.left -= +grant.spent;
      await grant.budget.save();

      grant.budget.account.spent += +grant.spent;
      grant.budget.account.left += +grant.granted;
      grant.budget.account.left -= +grant.spent;
      await grant.budget.account.save();

      return await grant.save();

    } else {
      if (data.notes) {
        grant[status] = data.notes;

        await this.mailerService.sendMail({
          to: grant.email,
          from: "RKNAdmin <donotreply@rkn.put.poznan.pl>",
          subject: "[RKNAdmin] Wprowadzono uwagi do wniosku",
          template: "base",
          context: {
            header: "Wprowadzono uwagi do wniosku",
            message: `Wprowadzono uwagi do Twojego wniosku: ${grant.subject}. Sprawdź je w panelu RKNAdmin i popraw wniosek.`
          }
        }).then(() => {
          return;
        }).catch((e) => {
          throw new InternalServerErrorException("Wystąpił błąd podczas wysyłania wiadomości e-mail");
        });
      } else if (data.rejectReason) {

        grant.budget.requested -= +grant.requested;
        grant.budget.left += +grant.requested;
        await grant.budget.save();

        grant.budget.account.requested -= +grant.requested;
        grant.budget.account.left += +grant.requested;
        await grant.budget.account.save();

        grant[`${status}Reason`] = data.rejectReason;

        await this.mailerService.sendMail({
          to: grant.email,
          from: "RKNAdmin <donotreply@rkn.put.poznan.pl>",
          subject: "[RKNAdmin] Wniosek odrzucony",
          template: "base",
          context: {
            header: "Wniosek odrzucony",
            message: `Twój wniosek: ${grant.subject} został odrzucony. Sprawdź powód odrzucenia w panelu RKNAdmin.`
          }
        }).then(() => {
          return;
        }).catch((e) => {
          throw new InternalServerErrorException("Wystąpił błąd podczas wysyłania wiadomości e-mail");
        });
      }
    }
    return await grant.save();
  }

  async addInvoice(id: number, amount: number) {
    const grant = await this.findOne(id);

    grant.spent += +amount;

    return await grant.save();
  }

  async update(id: number, updateGrantDto, user: User, files: Array<Express.Multer.File>) {
    const grant = await this.findOne(id);

    if ([GrantStatus.NotesRKN, GrantStatus.NotesZCO, GrantStatus.NotesR1].includes(grant.status)) {
      for (const prop in updateGrantDto) grant[prop] = updateGrantDto[prop];

      if (grant.status === GrantStatus.NotesZCO)
        grant.status = GrantStatus.CorrectedZCO;
      else if (grant.status === GrantStatus.NotesRKN)
        grant.status = GrantStatus.CorrectedRKN;
      else if (grant.status === GrantStatus.NotesR1)
        grant.status = GrantStatus.CorrectedR1;

      for (const file of files)
        await this.filesService.create(file, EntityType.Grant, id);

      if (updateGrantDto["requested"]) {
        grant.budget.requested -= grant.requested;
        grant.budget.left += grant.requested;
        grant.budget.requested += updateGrantDto["requested"];
        grant.budget.left -= updateGrantDto["requested"];
        await grant.budget.save();

        grant.budget.account.requested -= grant.requested;
        grant.budget.account.left += grant.requested;
        grant.budget.account.requested += updateGrantDto["requested"];
        grant.budget.account.left -= updateGrantDto["requested"];
        await grant.budget.account.save();

        grant.requested = updateGrantDto["requested"];
      }

      return await grant.save();
    } else throw new InternalServerErrorException("Wniosek nie wymaga edycji.");
  }

  async removeFile(user: User, grantId: number, fileId: number) {
    const grant = await this.findOne(grantId);

    if (
      (!user.societies.some(society => society.id === grant.society.id)
        && !["zco", "rkn"].includes(user.role))
    )
      throw new InternalServerErrorException("Brak uprawnień!");

    return await this.filesService.remove(fileId);
  }

  remove(id: number) {
    return `This action removes a #${id} grant`;
  }
}

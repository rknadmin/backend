import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
  UploadedFiles,
  UseFilters,
  UseGuards,
  UseInterceptors
} from "@nestjs/common";
import { GrantsService } from "./grants.service";
import { FilesInterceptor } from "@nestjs/platform-express";
import { AuthExceptionFilter } from "../common/filters/auth-exceptions.filter";
import { AuthenticatedGuard } from "../common/guards/authenticated.guard";
import { Roles } from "../common/decorators/roles.decorator";
import { User } from "../common/decorators/user.decorator";
import { User as UserEntity } from "../users/user.entity";
import GrantStatus from "./enums/grant-status.enum";
import MimeType from "../files/enums/mime-type.enum";
import { Response } from "express";

@Controller('api/grants')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class GrantsController {
    constructor(private readonly grantsService: GrantsService) {
    }

    @Post()
    @Roles('all')
    @UseInterceptors(FilesInterceptor('files', 10, {
        limits: {fileSize: 5000000},
    }))
    async create(@Res() res: Response, @User() user: UserEntity, @UploadedFiles() files: Array<Express.Multer.File>, @Body() createGrantDto){//: CreateGrantDto) {
        if(files.some(f => !Object.values(MimeType).includes(f.mimetype as MimeType) || f.size > 5000000))
            return res.status(HttpStatus.BAD_REQUEST).send('Niepoprawne rozszerzenie pliku lub za duży rozmiar');
        await this.grantsService.create(createGrantDto, user, files).then(() => {return res.status(201).send();}).catch(err => { console.log(err) });
    }

    @Get()
    @Roles('zco')
    findAll() {
        return this.grantsService.findAll();
    }

    @Get(':id')
    @Roles('rkn', 'zco')
    findOne(@Param('id') id: string) {
        return this.grantsService.findOne(+id);
    }

    @Put(':id/status/:status')
    @Roles('zco', 'rkn')
    updateStatus(@Param('id') id: string, @Param('status') status: GrantStatus, @Body() data) {
        return this.grantsService.updateStatus(+id, status, data);
    }

    @Post(':id/invoice')
    @Roles('zco')
    async addInvoice(@Param('id') id: string, @Body() data: { amount: string }) {
        return this.grantsService.addInvoice(+id, +data.amount);
    }

    @Put(':id')
    @Roles('all')
    @UseInterceptors(FilesInterceptor('files', 10, {
        limits: {fileSize: 5000000},
    }))
    async update(@Res() res: Response, @User() user: UserEntity, @Param('id') id: string, @UploadedFiles() files: Array<Express.Multer.File>, @Body() updateGrantDto) {
        if(files.some(f => !Object.values(MimeType).includes(f.mimetype as MimeType) || f.size > 5000000))
            return res.status(HttpStatus.BAD_REQUEST).send('Niepoprawne rozszerzenie pliku lub za duży rozmiar');
        await this.grantsService.update(+id, updateGrantDto, user, files).then(() => {return res.status(201).send();}).catch(err => { console.log(err) });
    }

    @Delete(':grantId/files/:fileId')
    @Roles('all')
    async removeFile(@User() user: UserEntity, @Param('grantId') grantId: string, @Param('fileId') fileId: string) {
        return this.grantsService.removeFile(user, +grantId, +fileId);
    }

    @Delete(':id')
    @Roles('zco')
    remove(@Param('id') id: string) {
        return this.grantsService.remove(+id);
    }
}

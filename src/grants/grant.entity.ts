import { TimeStampedEntity } from "../common/entities/time-stamped.entity";
import { Column, Entity, ManyToOne } from "typeorm";
import { IsEmail } from "class-validator";
import { User } from "../users/user.entity";
import { Society } from "../societies/society.entity";
import { Budget } from "../budgets/budget.entity";
import GrantStatus from "./enums/grant-status.enum";
import { ColumnNumericTransformer } from "../common/transformers/column-numeric.transformer";

@Entity()
export class Grant extends TimeStampedEntity {
    @Column()
    @IsEmail()
    email: string;

    @Column()
    phone: number;

    @Column()
    applicant: string;

    @ManyToOne((type) => User, (user) => user.grants, {
        onDelete: 'CASCADE',
    })
    user: User;

    @Column()
    subject: string;

    @Column('longtext')
    description: string;

    @Column('longtext')
    acquired: string;

    @Column('longtext')
    costs: string;

    @Column({type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer(),})
    requested: number;

    @Column({type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer(),})
    granted: number;

    @Column({type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer(),})
    spent: number;

    @Column('longtext')
    schedule: string;

    @Column('longtext')
    results: string;

    @Column({default: false})
    intellectualProperty: boolean;

    @Column('longtext',{nullable: true})
    notesRKN: string;

    @Column('longtext',{nullable: true})
    notesZCO: string;

    @Column('longtext',{nullable: true})
    notesR1: string;

    @Column('longtext',{nullable: true})
    rejectedZCOReason: string;

    @Column('longtext',{nullable: true})
    rejectedRKNReason: string;

    @Column('longtext', {nullable: true})
    rejectedR1Reason: string;

    @Column({default: false})
    correctedZCO: boolean;

    @Column({default: false})
    correctedRKN: boolean;

    @Column({default: false})
    correctedR1: boolean;

    @Column({default: GrantStatus.Submitted})
    status: GrantStatus;

    @Column({nullable: true})
    lastCorrection: Date;

    @ManyToOne((type) => Budget, (budget) => budget.grants, {
        onDelete: 'CASCADE',
    })
    budget: Budget;

    @ManyToOne((type) => Society, (society) => society.grants, {
        onDelete: 'CASCADE',
    })
    society: Society;
}

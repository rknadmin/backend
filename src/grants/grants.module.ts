import { Module } from "@nestjs/common";
import { GrantsService } from "./grants.service";
import { GrantsController } from "./grants.controller";
import { Grant } from "./grant.entity";
import { TypeOrmModule } from "@nestjs/typeorm";
import { SocietiesModule } from "../societies/societies.module";
import { BudgetsModule } from "../budgets/budgets.module";
import { FilesModule } from "../files/files.module";

@Module({
    imports: [TypeOrmModule.forFeature([Grant]), SocietiesModule, BudgetsModule, FilesModule],
    controllers: [GrantsController],
    providers: [GrantsService],
    exports: [GrantsService]
})
export class GrantsModule {
}

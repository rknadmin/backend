enum GrantStatus {
    // 1 - Wniosek złożony.
    Submitted = 'submitted',

    // 2.1 - Wniosek zaakceptowany przez ZCO, przekazano do RKN.
    AcceptedZCO = 'acceptedZCO',

    // 2.2 - ZCO przekazało uwagi, wniosek do poprawy.
    NotesZCO = 'notesZCO',

    // 2.2a - Wniosek poprawiony.
    CorrectedZCO = 'correctedZCO',

    // 2.3 - Wniosek odrzucony przez ZCO.
    RejectedZCO = 'rejectedZCO',

    // 3.1 - Wniosek zaakceptowany przez RKN, przekazano do ZCO.
    AcceptedRKN = 'acceptedRKN',

    // 3.2 - RKN przekazało uwagi, wniosek do poprawy.
    NotesRKN = 'notesRKN',

    // 3.2a - Wniosek poprawiony
    CorrectedRKN = 'correctedRKN',

    // 3.3 - Wniosek odrzucony przez RKN.
    RejectedRKN = 'rejectedRKN',

    // 4 - Oczekiwanie na dostarczenie wydrukowanego i podpisanego wniosku.
    Appointment = 'appointment',

    // 5 - Wydruk dostarczony, przekazano do R1.
    Delivered = 'delivered',

    // 6.1 - Wniosek odrzucony przez R1, powód: ...
    RejectedR1 = 'rejectedR1',

    // 6.2 - R1 przekazało uwagi.
    NotesR1 = 'notesR1',

    // 6.2a - Wniosek poprawiony.
    CorrectedR1 = 'correctedR1',

    // 6.3 - Wniosek zaakceptowany przez R1, możliwość rozliczania wniosku.
    Pending = 'pending',

    // 7 - Rozliczanie zakończone.
    Completed = 'completed',
}

export default GrantStatus;
import { ToBoolean } from "../../common/decorators/to-boolean";

export class CreateGrantDto {
    societyId!: number;
    budgetId!: number;
    phone!: number;
    subject!: string;
    description!: string;
    acquired!: string;
    costs!: string;
    requested!: number;
    granted!: number;
    schedule!: string;
    results!: string;
    applicant?: string;
    email?: string;

    @ToBoolean()
    intellectualProperty?: boolean;
}

import { Module } from "@nestjs/common";
import { FundingsService } from "./fundings.service";
import { FundingsController } from "./fundings.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Funding } from "./funding.entity";
import { SocietiesModule } from "../societies/societies.module";
import { BudgetsModule } from "../budgets/budgets.module";
import { FilesModule } from "../files/files.module";

@Module({
    imports: [TypeOrmModule.forFeature([Funding]), SocietiesModule, BudgetsModule, FilesModule],
    controllers: [FundingsController],
    providers: [FundingsService],
    exports: [FundingsService]
})
export class FundingsModule {
}

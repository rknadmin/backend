enum FundingStatus {
    // 1 - Wniosek złożony.
    Submitted = 'submitted',

    // 2.1 - Wniosek zaakceptowany przez ZCO, oczekiwanie na dostarczenie wydrukowanego i podpisanego wniosku.
    AcceptedZCO = 'acceptedZCO',

    // 2.2 - ZCO przekazało uwagi.
    NotesZCO = 'notesZCO',

    // 2.2a - Wniosek poprawiony.
    CorrectedZCO = 'correctedZCO',

    // 2.3 - Wniosek odrzucony przez ZCO.
    RejectedZCO = 'rejectedZCO',

    // 3 - Wydruk dostarczony, przekazano do R1.
    Delivered = 'delivered',

    // 4.1 - Wniosek odrzucony przez R1, powód: ...
    RejectedR1 = 'rejectedR1',

    // 4.2 - Wniosek zaakceptowany przez R1, możliwość rozliczania wniosku.
    Pending = 'pending',

    // 4.3 - R1 przekazało uwagi.
    NotesR1 = 'notesR1',

    // 4.3a - Wniosek poprawiony.
    CorrectedR1 = 'correctedR1',

    // 5 - Rozliczanie zakończone.
    Completed = 'completed',
}

export default FundingStatus;
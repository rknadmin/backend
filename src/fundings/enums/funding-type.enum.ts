enum FundingType {
    Statutory = 'statutory',
    Additional = 'additional',
    Sponsorship = 'sponsorship',
    Other = 'other'
}

export default FundingType;
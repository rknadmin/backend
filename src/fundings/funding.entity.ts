import { TimeStampedEntity } from "../common/entities/time-stamped.entity";
import { Column, Entity, ManyToOne } from "typeorm";
import { IsEmail } from "class-validator";
import { User } from "../users/user.entity";
import FundingType from "./enums/funding-type.enum";
import { Budget } from "../budgets/budget.entity";
import { Society } from "../societies/society.entity";
import FundingStatus from "./enums/funding-status.enum";
import { ColumnNumericTransformer } from "../common/transformers/column-numeric.transformer";

@Entity()
export class Funding extends TimeStampedEntity {
    @Column()
    @IsEmail()
    email: string;

    @Column()
    phone: number;

    @Column()
    applicant: string;

    @ManyToOne((type) => User, (user) => user.fundings, {
        onDelete: 'CASCADE',
    })
    user: User;

    @Column()
    subject: string;

    @Column('longtext')
    goal: string;

    @Column('longtext')
    description: string;

    @Column({ default: FundingType.Other})
    type: FundingType;

    @Column({type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer(),})
    requested: number;

    @Column({type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer(),})
    granted: number;

    @Column({type: 'numeric', precision: 10, scale: 2, default: 0, transformer: new ColumnNumericTransformer(),})
    spent: number;

    @Column('longtext')
    costs: string;

    @Column()
    date: string;

    @Column({default: false})
    intellectualProperty: boolean;

    @Column('longtext', {nullable: true})
    notesZCO: string;

    @Column('longtext', {nullable: true})
    notesR1: string;

    @Column('longtext', {nullable: true})
    rejectedZCOReason: string;

    @Column('longtext', {nullable: true})
    rejectedR1Reason: string;

    @Column({default: false})
    correctedZCO: boolean;

    @Column({default: false})
    correctedR1: boolean;

    @Column({default: 'submitted'})
    status: FundingStatus;

    @Column({nullable: true})
    lastCorrection: Date;

    @ManyToOne((type) => Budget, (budget) => budget.fundings, {
        onDelete: 'CASCADE',
    })
    budget: Budget;

    @ManyToOne((type) => Society, (society) => society.fundings, {
        onDelete: 'CASCADE',
    })
    society: Society;
}

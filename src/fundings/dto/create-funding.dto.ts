import { ToBoolean } from "../../common/decorators/to-boolean";

export class CreateFundingDto {
    budgetId!: number;
    societyId!: number;
    phone!: number;
    subject!: string;
    goal!: string;
    description!: string;
    requested!: number;
    costs!: string;
    date!: string;
    applicant?: string;
    email?: string;

    @ToBoolean()
    intellectualProperty?: boolean;
}

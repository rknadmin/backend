import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Post,
  Put,
  Res,
  UploadedFiles,
  UseFilters,
  UseGuards,
  UseInterceptors
} from "@nestjs/common";
import { FundingsService } from "./fundings.service";
import { FilesInterceptor } from "@nestjs/platform-express";
import { AuthExceptionFilter } from "../common/filters/auth-exceptions.filter";
import { AuthenticatedGuard } from "../common/guards/authenticated.guard";
import { Roles } from "../common/decorators/roles.decorator";
import { User } from "../common/decorators/user.decorator";
import { User as UserEntity } from "../users/user.entity";
import FundingStatus from "./enums/funding-status.enum";
import MimeType from "../files/enums/mime-type.enum";
import { Response } from "express";

@Controller('api/fundings')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class FundingsController {
    constructor(private readonly fundingsService: FundingsService) {
    }

    @Post()
    @Roles('all')
    @UseInterceptors(FilesInterceptor('files', 10, {
        limits: {fileSize: 5000000},
    }))
    async create(@Res() res: Response, @User() user: UserEntity, @UploadedFiles() files: Array<Express.Multer.File>, @Body() createFundingDto) {//: CreateFundingDto) {
        if(files.some(f => !Object.values(MimeType).includes(f.mimetype as MimeType) || f.size > 5000000))
            return res.status(HttpStatus.BAD_REQUEST).send('Niepoprawne rozszerzenie pliku lub za duży rozmiar');
        await this.fundingsService.create(createFundingDto, user, files).then(() => {return res.status(201).send();}).catch(err => { console.log(err) });
    }

    @Get()
    @Roles('zco')
    findAll() {
        return this.fundingsService.findAll();
    }

    @Get(':id')
    @Roles('zco')
    findOne(@Param('id') id: string) {
        return this.fundingsService.findOne(+id);
    }

    @Put(':id/status/:status')
    @Roles('zco')
    updateStatus(@Param('id') id: string, @Param('status') status: FundingStatus, @Body() data) {
        return this.fundingsService.updateStatus(+id, status, data);
    }

    @Post(':id/invoice')
    @Roles('zco')
    async addInvoice(@Param('id') id: string, @Body() data: { amount: string }) {
        return await this.fundingsService.addInvoice(+id, +data.amount);
    }

    @Put(':id')
    @Roles('all')
    @UseInterceptors(FilesInterceptor('files', 10, {
        limits: {fileSize: 5000000},
    }))
    async update(@Res() res: Response, @User() user: UserEntity, @Param('id') id: string, @UploadedFiles() files: Array<Express.Multer.File>, @Body() updateFundingDto) {
        if(files.some(f => !Object.values(MimeType).includes(f.mimetype as MimeType) || f.size > 5000000))
            return res.status(HttpStatus.BAD_REQUEST).send('Niepoprawne rozszerzenie pliku lub za duży rozmiar');
        await this.fundingsService.update(+id, updateFundingDto, user, files).then(() => {return res.status(201).send();}).catch(err => { console.log(err) });
    }

    @Delete(':fundingId/files/:fileId')
    @Roles('all')
    async removeFile(@User() user: UserEntity, @Param('fundingId') fundingId: string, @Param('fileId') fileId: string) {
        return this.fundingsService.removeFile(user, +fundingId, +fileId);
    }

    @Delete(':id')
    @Roles('zco')
    remove(@Param('id') id: string) {
        return this.fundingsService.remove(+id);
    }
}

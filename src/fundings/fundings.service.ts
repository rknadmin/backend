import { Injectable, InternalServerErrorException } from "@nestjs/common";
import { CreateFundingDto } from "./dto/create-funding.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { Funding } from "./funding.entity";
import { SocietiesService } from "../societies/societies.service";
import { BudgetsService } from "../budgets/budgets.service";
import { User } from "../users/user.entity";
import FundingStatus from "./enums/funding-status.enum";
import AccountType from "../accounts/enums/account-type.enum";
import FundingType from "./enums/funding-type.enum";
import { FilesService } from "../files/files.service";
import EntityType from "../files/enums/entity-type.enum";
import { MailerService } from "@nestjs-modules/mailer";
import moment = require("moment");


@Injectable()
export class FundingsService {
  constructor(
    @InjectRepository(Funding) private fundingsRepository: Repository<Funding>,
    private budgetsService: BudgetsService,
    private societiesService: SocietiesService,
    private filesService: FilesService,
    private readonly mailerService: MailerService
  ) {
  }

  async create(createFundingDto: CreateFundingDto, user: User, files: Array<Express.Multer.File>) {
    const { societyId, budgetId, ...data } = createFundingDto;

    const society = await this.societiesService.findOne(+societyId);
    const budget = await this.budgetsService.findOne(+budgetId);

    if ((user.societies.some(s => s.id === +societyId) && society.budgets.some(b => b.id === +budgetId)) || user.role === "zco") {
      if (budget.account.type !== AccountType.Grant && (budget.account.unlimited || !budget.account.amountVisible || data.requested <= budget.left)) {
        if (moment(data.date).isBefore(moment(), "day")) throw new InternalServerErrorException("Nie można wskazać daty wstecz!");

        const funding = this.fundingsRepository.create({
          ...data,
          user: user,
          type: FundingType[budget.account.type],
          granted: 0,
          status: FundingStatus.Submitted,
          budget,
                    society
                });

                if (!data['email'] || data['email'] === '') funding['email'] = user.email;
                if (!data['applicant'] || data['applicant'] === '') funding['applicant'] = user.name;

                try {
                  const savedFunding = await funding.save();

                  budget.requested += +data.requested;
                  budget.left -= +data.requested;
                  await budget.save();

                  budget.account.requested += +data.requested;
                  budget.account.left -= +data.requested;
                  await budget.account.save();

                  for (const file of files)
                    await this.filesService.create(file, EntityType.Funding, savedFunding.id);

                  return savedFunding;
                } catch (e) {
                    throw new InternalServerErrorException(e);
                }
            } else throw new InternalServerErrorException('Przekroczono budżet!');
        } else throw new InternalServerErrorException('Brak uprawnień!');

        throw new InternalServerErrorException('Wystąpił błąd!');
    }

  async findAll() {
    return await this.fundingsRepository.createQueryBuilder("funding")
      .leftJoinAndSelect("funding.budget", "budget")
      .leftJoinAndSelect("budget.account", "account")
      .leftJoinAndSelect("funding.society", "society")
      .getMany();
  }

  async findOne(id: number) {
    return await this.fundingsRepository.createQueryBuilder("funding")
      .where("funding.id = :id", { id })
      .leftJoinAndSelect("funding.user", "user")
      .leftJoinAndSelect("funding.budget", "budget")
      .leftJoinAndSelect("budget.account", "account")
      .leftJoinAndSelect("funding.society", "society")
      .leftJoinAndSelect("society.faculty", "faculty")
      .getOne();
  }

  async updateStatus(id: number, status: FundingStatus, data) {
    const funding = await this.findOne(id);

    funding.status = status;

    if (status === FundingStatus.AcceptedZCO) {

      if (data.appointmentDate) {
        const date = moment(data.appointmentDate).format("DD.MM [o godz.] HH:mm");

        await this.mailerService.sendMail({
          to: funding.email,
          from: "RKNAdmin <donotreply@rkn.put.poznan.pl>",
          subject: "[RKNAdmin] Dostarcz wydrukowany wniosek",
          template: "base",
          context: {
            header: "Wniosek zaakceptowany przez ZCO",
            message: `Twój wniosek: ${funding.subject} został zaakceptowany przez ZCO. Wydrukuj wniosek i przynieś do ZCO (pamiętaj o wymaganych podpisach), proponowany termin dostarczenia to: ${date}. W celu zmiany terminu skontakuj się z Iwoną Michałowską (iwona.michalowska@put.poznan.pl).`
          }
        }).then(() => {
          return;
        }).catch((e) => {
          throw new InternalServerErrorException("Wystąpił błąd podczas wysyłania wiadomości e-mail");
        });
      }

    } else if (status === FundingStatus.Pending) {

      await this.mailerService.sendMail({
        to: funding.email,
        from: "RKNAdmin <donotreply@rkn.put.poznan.pl>",
        subject: "[RKNAdmin] Wniosek zaakceptowany",
        template: "base",
        context: {
          header: "Wniosek zaakceptowany",
          message: `Twój wniosek: ${funding.subject} został zaakceptowany. Możesz rozpocząć jego rozliczanie.`
        }
      }).then(() => {
        return;
      }).catch((e) => {
        throw new InternalServerErrorException("Wystąpił błąd podczas wysyłania wiadomości e-mail");
      });

      funding.budget.granted += +data.granted;
      funding.budget.left += +funding.requested;
      funding.budget.left -= +data.granted;
      await funding.budget.save();

      funding.budget.account.granted += +data.granted;
      funding.budget.account.left += +funding.requested;
      funding.budget.account.left -= +data.granted;
      await funding.budget.account.save();

      funding.granted = +data.granted;
      return await funding.save();

    } else if (status === FundingStatus.Completed) {

      await this.mailerService.sendMail({
        to: funding.email,
        from: "RKNAdmin <donotreply@rkn.put.poznan.pl>",
        subject: "[RKNAdmin] Rozliczanie wniosku zakończone",
        template: "base",
        context: {
          header: "Rozliczanie wniosku zakończone",
          message: `Rozliczanie Twojego wniosku: ${funding.subject} zostało zakończone. Niewydana kwota została ponownie włączona do dostępnego budżetu Twojej organizacji.`
        }
      }).then(() => {
        return;
      }).catch((e) => {
        throw new InternalServerErrorException("Wystąpił błąd podczas wysyłania wiadomości e-mail");
      });

      funding.budget.spent += +funding.spent;
      funding.budget.left += +funding.granted;
      funding.budget.left -= +funding.spent;
      await funding.budget.save();

      funding.budget.account.spent += +funding.spent;
      funding.budget.account.left += +funding.granted;
      funding.budget.account.left -= +funding.spent;
      await funding.budget.account.save();

      return await funding.save();

    } else {
      if (data.notes) {
        funding[status] = data.notes;

        await this.mailerService.sendMail({
          to: funding.email,
          from: "RKNAdmin <donotreply@rkn.put.poznan.pl>",
          subject: "[RKNAdmin] Wprowadzono uwagi do wniosku",
          template: "base",
          context: {
            header: "Wprowadzono uwagi do wniosku",
            message: `Wprowadzono uwagi do Twojego wniosku: ${funding.subject}. Sprawdź je w panelu RKNAdmin i popraw wniosek.`
          }
        }).then(() => {
          return;
        }).catch((e) => {
          throw new InternalServerErrorException("Wystąpił błąd podczas wysyłania wiadomości e-mail");
        });
      } else if (data.rejectReason) {
        funding.budget.requested -= +funding.requested;
        funding.budget.left += +funding.requested;
        await funding.budget.save();

        funding.budget.account.requested -= +funding.requested;
        funding.budget.account.left += +funding.requested;
        await funding.budget.account.save();

        funding[`${status}Reason`] = data.rejectReason;

        await this.mailerService.sendMail({
          to: funding.email,
          from: "RKNAdmin <donotreply@rkn.put.poznan.pl>",
          subject: "[RKNAdmin] Wniosek odrzucony",
          template: "base",
          context: {
            header: "Wniosek odrzucony",
            message: `Twój wniosek: ${funding.subject} został odrzucony. Sprawdź powód odrzucenia w panelu RKNAdmin.`
          }
        }).then(() => {
          return;
        }).catch((e) => {
          throw new InternalServerErrorException("Wystąpił błąd podczas wysyłania wiadomości e-mail");
        });
      }
    }
    return await funding.save();
  }

  async addInvoice(id: number, amount: number) {
    const funding = await this.findOne(id);

    funding.spent += +amount;

    return await funding.save();
  }

  async update(id: number, updateFundingDto, user: User, files: Array<Express.Multer.File>) {
    const funding = await this.findOne(id);

    if ([FundingStatus.NotesZCO, FundingStatus.NotesR1].includes(funding.status)) {
      for (const prop in updateFundingDto) funding[prop] = updateFundingDto[prop];

      if (funding.status === FundingStatus.NotesZCO)
        funding.status = FundingStatus.CorrectedZCO;
      else if (funding.status === FundingStatus.NotesR1)
        funding.status = FundingStatus.CorrectedR1;

      for (const file of files)
        await this.filesService.create(file, EntityType.Funding, id);

      if (updateFundingDto["requested"]) {
        funding.budget.requested -= funding.requested;
        funding.budget.left += funding.requested;
        funding.budget.requested += updateFundingDto["requested"];
        funding.budget.left -= updateFundingDto["requested"];
        await funding.budget.save();

        funding.budget.account.requested -= funding.requested;
        funding.budget.account.left += funding.requested;
        funding.budget.account.requested += updateFundingDto["requested"];
        funding.budget.account.left -= updateFundingDto["requested"];
        await funding.budget.account.save();

        funding.requested = updateFundingDto["requested"];
      }
      return await funding.save();
    } else throw new InternalServerErrorException("Wniosek nie wymaga edycji.");
  }

  async removeFile(user: User, fundingId: number, fileId: number) {
    const funding = await this.findOne(fundingId);

    if (
      (!user.societies.some(society => society.id === funding.society.id)
        && user.role !== "zco")
    )
      throw new InternalServerErrorException("Brak uprawnień!");

    return await this.filesService.remove(fileId);
  }

  remove(id: number) {
    return `This action removes a #${id} funding`;
  }
}

import {
    Body,
    Controller,
    Get, Inject,
    InternalServerErrorException,
    Param,
    Post,
    Render,
    Req,
    Request,
    Res,
    UseFilters,
    UseGuards
} from '@nestjs/common';
import {AuthService} from './auth.service';
import {SocietiesService} from '../societies/societies.service';
import {Response} from 'express';
import {LoginGuard} from '../common/guards/login.guard';
import {AuthenticatedGuard} from '../common/guards/authenticated.guard';
import {RegisterUserDto} from '../users/dto/register-user.dto';
import {AuthExceptionFilter} from '../common/filters/auth-exceptions.filter';
import {WINSTON_MODULE_NEST_PROVIDER} from "nest-winston";

@Controller('auth')
export class AuthController {
    constructor(
        private authService: AuthService,
        private societiesService: SocietiesService,
        @Inject(WINSTON_MODULE_NEST_PROVIDER) private readonly logger
    ) {
        this.logger.setContext(AuthController.name)

    }

    @UseFilters(AuthExceptionFilter)
    @Get('/')
    @Render('auth/pages/login')
    index(@Request() req, @Res() res) {
        if (req.user) {
            this.logger.log(`Redirecting logged in user to admin panel.`)
            return res.redirect('/admin');
        }
        else
            return {title: 'Logowanie'};
    }

    @UseFilters(AuthExceptionFilter)
    @Get('/register')
    @Render('auth/pages/register')
    async registerForm(@Request() req, @Res() res) {
        if (req.user) {
            this.logger.log(`Redirecting logged in user to admin panel.`)
            return res.redirect('/admin');
        } else {
            const societies = await this.societiesService.findAll();
            return {title: 'Rejestracja', societies};
        }
    }

    @Post('/register')
    async register(@Res() res: Response, @Body() registerUserDto: RegisterUserDto) {
        if (await this.authService.getUser(registerUserDto.email))
            throw new InternalServerErrorException(`User with email ${registerUserDto.email} already exists.`);
        await this.authService.registerUser(registerUserDto);
        return res.status(204).send();
    }

    @UseFilters(AuthExceptionFilter)
    @Get('/verify/:email')
    @Render('auth/pages/email-verify')
    async verifyEmail(@Req() req, @Res() res: Response, @Param('email') email: string) {
        if (req.user) {
            this.logger.log(`Redirecting logged in user to admin panel.`)
            return res.redirect('/admin');
        } else {
            return {title: 'Zweryfikuj adres e-mail', email}
        }
    }

    @Post('/verify/:email/:code')
    async verifyEmailCode(@Res() res: Response, @Param('email') email: string, @Param('code') code: string) {
        return await this.authService.verifyEmail(email, code);
    }

    @Post('/login')
    async generateLoginToken(@Res() res: Response, @Body() data: { email: string, index: string }) {
        await this.authService.generateLoginToken(data.email, +data.index);
        return res.status(204).send();
    }

    @UseFilters(AuthExceptionFilter)
    @Get('/login/verify/:email')
    @Render('auth/pages/login-verify')
    async verifyLogin(@Req() req, @Res() res: Response, @Param('email') email: string) {
        if (req.user) {
            this.logger.log(`Redirecting logged in user to admin panel.`)
            return res.redirect('/admin');
        } else {
            return {title: 'Logowanie', email}
        }
    }

    @UseGuards(LoginGuard)
    @Post('/login/verify/token')
    loginToken(@Res() res: Response, @Body() body: { email: string, emailToken: string }) {
        return res.status(204).send();
    }

    @UseFilters(AuthExceptionFilter)
    @UseGuards(AuthenticatedGuard)
    @Get('logout')
    logout(@Request() req, @Res() res: Response) {
        this.logger.log(`Logging out user ${req.user.email}.`)
        delete req.user;
        req.session.cookie.maxAge = 0;
        return res.redirect('/auth');
    }
}

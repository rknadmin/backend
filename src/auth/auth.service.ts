import {Inject, Injectable, InternalServerErrorException, UnauthorizedException} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {EmailVerification} from './email-verification.entity';
import {MailerService} from '@nestjs-modules/mailer';
import {UsersService} from '../users/users.service';
import {LoginVerification} from './login-verification.entity';
import {RegisterUserDto} from '../users/dto/register-user.dto';
import {WINSTON_MODULE_NEST_PROVIDER} from 'nest-winston';
import {User} from '../users/user.entity';

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(EmailVerification) private emailVerificationRepository: Repository<EmailVerification>,
        @InjectRepository(LoginVerification) private loginVerificationRepository: Repository<LoginVerification>,
        private readonly mailerService: MailerService,
        private readonly usersService: UsersService,
        @Inject(WINSTON_MODULE_NEST_PROVIDER) private readonly logger
    ) {
        this.logger.setContext(AuthService.name)
    }

    /**
     * Generates a random 6-digit token.
     * @returns {string} - 6-digit token
     */
    randomizeToken(): string {
        const min = 0, max = 999999;
        const rand = Math.floor(Math.random() * (max - min + 1) + min);
        return String(rand).padStart(6, '0');
    }

    /**
     * Returns a user with given email from database.
     * @param email - user's email
     * @returns {Promise<User>} - user with given email
     */
    async getUser(email: string): Promise<User> {
        return await this.usersService.findUser(email);
    }

    /**
     * Registers a new user in database and sends an email with verification token.
     * @param registerUserDto - user's data
     */
    async registerUser(registerUserDto: RegisterUserDto): Promise<any> {
        await this.usersService.register(registerUserDto);

        const email = registerUserDto.email;
        const emailToken = this.randomizeToken();

        const emailVerification = await this.emailVerificationRepository.save({
            email,
            emailToken
        });

        if (emailVerification) {
            await this.mailerService.sendMail({
                to: email,
                from: 'RKNAdmin <donotreply@rkn.put.poznan.pl>',
                subject: '[RKNAdmin] Rejestracja nowego konta',
                template: 'base',
                context: {
                    header: 'Rejestracja nowego konta',
                    message: 'Potwierdzamy rejestrację w systemie RKNAdmin. Twój kod do weryfikacji adresu e-mail to:',
                    token: emailToken,
                    url: 'https://rkn.put.poznan.pl/auth/verify/' + email
                }
            }).then(() => {
                this.logger.debug(`Sent an email to ${email}.`);
                return;
            }).catch((e) => {
                this.logger.error(e);
                throw new InternalServerErrorException(`There was an error while sending an email to ${email}.`);
            });
        }
    }

    /**
     * Generates a login token and sends an email with verification token.
     * @param email - user's email
     * @param index - user's index number
     */
    async generateLoginToken(email: string, index: number): Promise<any> {
        const user = await this.usersService.findUser(email);

        if (user && user.index === index) {
            if (user.verified) {
                const emailToken = this.randomizeToken();
                this.logger.debug(`Generated login token for ${email}: ${emailToken}`);

                const foundLoginVerification = await this.loginVerificationRepository.findOneBy({email});
                if (foundLoginVerification) await this.loginVerificationRepository.delete(foundLoginVerification.id);

                const loginVerification = await this.loginVerificationRepository.save({
                    email,
                    emailToken
                });

                if (loginVerification) {
                    await this.mailerService.sendMail({
                        to: email,
                        from: 'RKNAdmin <donotreply@rkn.put.poznan.pl>',
                        subject: '[RKNAdmin] Logowanie do panelu administracyjnego',
                        template: 'base',
                        context: {
                            header: 'Logowanie',
                            message: 'Wysłano prośbę o logowanie do systemu. Twój kod do logowania to:',
                            token: emailToken,
                            url: 'https://rkn.put.poznan.pl/auth/login/verify/' + email
                        }
                    }).then(() => {
                        this.logger.debug(`Sent an email to ${email}.`);
                        return;
                    }).catch((e) => {
                        this.logger.error(e);
                        throw new InternalServerErrorException(`There was an error while sending an email to ${email}.`);
                    });
                } else throw new InternalServerErrorException(`There was an error while generating a login token for ${email}.`);
            } else throw new InternalServerErrorException(`User with email ${email} is not verified yet.`);
        } else throw new InternalServerErrorException(`User with email ${email} does not exist.`);
    }

    /**
     * Verifies a user's email address.
     * @param email - user's email
     * @param code - verification code
     */
    async verifyEmail(email: string, code: string): Promise<any> {
        const emailVerification = await this.emailVerificationRepository.findOneBy({email});

        if (emailVerification) {
            // Check if code expired
            const timeDiff = new Date().getTime() - emailVerification.createdAt.getTime();
            if (timeDiff / 60000 < 75) {

                // Check if code is valid
                if (emailVerification.emailToken === code) {
                    const user = await this.usersService.findUser(email);
                    user.emailVerified = true;
                    user.save();

                    await this.emailVerificationRepository.delete(emailVerification.id);
                    this.logger.debug(`User ${email} verified their email address.`);
                    return;
                } else {
                    emailVerification.tries++;

                    if (emailVerification.tries < 4) {
                        await emailVerification.save();
                        throw new InternalServerErrorException(`Invalid code for ${email}.`);
                    } else {
                        await this.emailVerificationRepository.delete(emailVerification.id);
                        throw new InternalServerErrorException(`Too many tries for ${email}.`);
                    }
                }
            } else {
                await this.emailVerificationRepository.delete(emailVerification.id);
                await this.usersService.findUser(email).then(async (user) => await this.usersService.remove(user.id));
                throw new InternalServerErrorException(`Code expired for ${email}. Register again.`);
            }
        }
        throw new InternalServerErrorException(`User with email ${email} does not exist.`);
    }

    /**
     * Validates a user's login token.
     * @param email - user's email
     * @param emailToken - login token
     */
    async validateUser(email, emailToken): Promise<User> {
        const loginVerification = await this.loginVerificationRepository.findOneBy({email});

        if (loginVerification) {
            // Check if code expired
            const timeDiff = new Date().getTime() - loginVerification.createdAt.getTime();
            if (timeDiff / 60000 < 75) {
                // Check if code is valid
                if (loginVerification.emailToken === emailToken) {
                    await this.loginVerificationRepository.delete(loginVerification.id);

                    const user = await this.usersService.findUser(email);
                    user.lastLogin = new Date().toISOString();

                    this.logger.debug(`User ${email} logged in.`);
                    return await user.save();
                } else {
                    loginVerification.tries++;
                    if (loginVerification.tries < 4) {
                        await loginVerification.save();
                        throw new UnauthorizedException(`Invalid code for ${email}.`)
                    } else {
                        await this.loginVerificationRepository.delete(loginVerification.id);
                        throw new UnauthorizedException(`Too many tries for ${email}. Log in again.`)
                    }
                }
            } else {
                await this.loginVerificationRepository.delete(loginVerification.id);
                throw new UnauthorizedException(`Code expired for ${email}. Log in again.`);
            }
        } else throw new UnauthorizedException(`User with email ${email} does not exist.`);
    }
}

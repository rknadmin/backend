import { TimeStampedEntity } from '../common/entities/time-stamped.entity';
import { Column, Entity } from 'typeorm';

@Entity()
export class LoginVerification extends TimeStampedEntity {
    @Column()
    email: string;

    @Column()
    emailToken: string;

    @Column({default: 0})
    tries: number;
}

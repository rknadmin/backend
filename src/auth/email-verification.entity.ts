import { TimeStampedEntity } from '../common/entities/time-stamped.entity';
import { Column, Entity } from 'typeorm';

@Entity()
export class EmailVerification extends TimeStampedEntity {
    @Column({unique: true})
    email: string;

    @Column()
    emailToken: string;

    @Column({default: 0})
    tries: number;
}

import { PassportStrategy } from '@nestjs/passport';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { Strategy } from 'passport-local';
import { AuthService } from './auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
    constructor(private readonly authService: AuthService) {
        super({
            usernameField: 'email',
            passwordField: 'emailToken'
        });
    }

    async validate(email: string, emailToken: string) {
        const user = await this.authService.validateUser(email, emailToken);

        if (!user)
            throw new UnauthorizedException();

        return user;
    }
}

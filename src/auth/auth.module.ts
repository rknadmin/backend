import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { PassportModule } from '@nestjs/passport';
import { LocalStrategy } from './local.strategy';
import { SessionSerializer } from './session.serializer';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EmailVerification } from './email-verification.entity';
import { LoginVerification } from './login-verification.entity';
import { AuthController } from './auth.controller';
import { SocietiesModule } from '../societies/societies.module';

@Module({
    imports: [SocietiesModule, UsersModule, PassportModule, TypeOrmModule.forFeature([EmailVerification, LoginVerification])],
    providers: [AuthService, LocalStrategy, SessionSerializer],
    exports: [AuthService],
    controllers: [AuthController]
})
export class AuthModule {
}

import { Column, Entity } from "typeorm";
import { TimeStampedEntity } from "../common/entities/time-stamped.entity";
import MimeType from "./enums/mime-type.enum";
import EntityType from "./enums/entity-type.enum";

@Entity()
export class File extends TimeStampedEntity {
  @Column({ nullable: true })
  name: string;
  @Column()
  originalName: string;
  @Column()
  mimetype: MimeType;
  @Column()
  entity: EntityType;
  @Column()
  entityId: number;
  @Column({ nullable: true })
  path: string;

  constructor(
    originalName: string,
    mimetype: MimeType,
    entity: EntityType,
    entityId: number
  ) {
    super();
    this.originalName = originalName;
    this.mimetype = mimetype;
    this.entity = entity;
    this.entityId = entityId;
  }
}

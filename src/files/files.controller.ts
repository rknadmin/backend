import { Body, Controller, Get, Param, Patch } from "@nestjs/common";
import { FilesService } from "./files.service";
import { UpdateFileDto } from "./dto/update-file.dto";

@Controller("files")
export class FilesController {
  constructor(private readonly filesService: FilesService) {
  }

  @Get()
  findAll() {
    return this.filesService.findAll();
  }

  @Get(":id")
  findOne(@Param("id") id: string) {
    return this.filesService.findOne(+id);
  }

  @Patch(":id")
  update(@Param("id") id: string, @Body() updateFileDto: UpdateFileDto) {
    return this.filesService.update(+id, updateFileDto);
  }
}

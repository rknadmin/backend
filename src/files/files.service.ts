import { Injectable, InternalServerErrorException } from "@nestjs/common";
import { UpdateFileDto } from "./dto/update-file.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { File } from "./file.entity";
import EntityType from "./enums/entity-type.enum";
import MimeType from "./enums/mime-type.enum";
import * as fs from "fs";

@Injectable()
export class FilesService {
  constructor(
    @InjectRepository(File) private readonly filesRepository: Repository<File>
  ) {
  }

  async create(file: Express.Multer.File, entityType: EntityType, entityId: number) {
    if (entityType === EntityType.Society) {
      const file = await this.filesRepository.createQueryBuilder("file")
        .where("entity = :entityType", { entityType })
        .andWhere("entityId = :entityId", { entityId })
        .getOne();
      if (file) await this.remove(file.id);
    }

    const createdFile = new File(file.originalname, file.mimetype as MimeType, entityType as EntityType, entityId);
    const savedFile = await this.filesRepository.create(createdFile).save();

    savedFile.originalName = file.originalname.replace(/_/g, "-");
    savedFile.name = `${savedFile.entityId}-${savedFile.id}-${savedFile.originalName}`;
    savedFile.path = `/uploads/${savedFile.entity}/${savedFile.name}`;

    const updatedFile = await savedFile.save();

    fs.createWriteStream("/usr/src/app/public" + updatedFile.path).write(file.buffer);
    return updatedFile;
  }

  async findFilesByEntity(entityType: EntityType, entityId: number) {
    return await this.filesRepository.createQueryBuilder("file")
      .where("entity = :entityType", { entityType })
      .andWhere("entityId = :entityId", { entityId })
      .getMany();
  }

  findAll() {
    return `This action returns all files`;
  }

  async findOne(id: number) {
    return await this.filesRepository.findOneBy({ id });
  }

  update(id: number, updateFileDto: UpdateFileDto) {
    return `This action updates a #${id} file`;
  }

  async remove(id: number) {
    const file = await this.findOne(id);

    try {
      fs.unlinkSync("/usr/src/app/public" + file.path);
      return await this.filesRepository.remove(file);
    } catch (e) {
      return new InternalServerErrorException(e);
    }
  }
}

enum MimeType {
  ImageJPEG = "image/jpeg",
  ImagePNG = "image/png",
  ImageBMP = "image/bmp",
  PDF = "application/pdf",
  SVG = "image/svg+xml"
}

export default MimeType;

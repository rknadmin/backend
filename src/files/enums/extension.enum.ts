enum Extension {
  JPEG = "jpeg",
  JPG = "jpg",
  PNG = "png",
  BMP = "bmp",
  PDF = "pdf",
  SVG = "svg"
}

export default Extension;

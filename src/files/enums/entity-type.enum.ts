enum EntityType {
  Society = "society",
  Grant = "grant",
  Funding = "funding",
}

export default EntityType;

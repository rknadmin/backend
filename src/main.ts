import {NestFactory} from "@nestjs/core";
import {AppModule} from "./app.module";
import {ValidationPipe} from "@nestjs/common";
import {NestExpressApplication} from "@nestjs/platform-express";
import {join} from "path";
import * as methodOverride from "method-override";
import "reflect-metadata";
import * as session from "express-session";
import * as passport from "passport";
import flash = require("connect-flash");
import { WINSTON_MODULE_NEST_PROVIDER } from 'nest-winston';

async function bootstrap() {
    const app = await NestFactory.create<NestExpressApplication>(AppModule);
    app.useLogger(app.get(WINSTON_MODULE_NEST_PROVIDER));
    app.useStaticAssets(join(__dirname, '..', 'public'));
    app.setBaseViewsDir(join(__dirname, '..', 'views'));
    app.setViewEngine('pug');

    app.use(
        session({
            secret: process.env.SECRET,
            resave: false,
            rolling: true,
            saveUninitialized: false,
            cookie: {maxAge: +process.env.EXPIRES_IN * 1000 * 60}
        }),
    );

    app.use(methodOverride('_method'));
    app.use(passport.initialize());
    app.use(passport.session());
    app.use(flash());

    app.enableCors();
    app.useGlobalPipes(new ValidationPipe({
        transform: true,
        forbidNonWhitelisted: true,
        validateCustomDecorators: true,
        disableErrorMessages: false,
    }))

    await app.listen(process.env.PORT || 8080);
}

bootstrap();

import {MiddlewareConsumer, Module, NestModule} from "@nestjs/common";
import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConfigModule } from "@nestjs/config";
import { UsersModule } from "./users/users.module";
import { SocietiesModule } from "./societies/societies.module";
import { FacultiesModule } from "./faculties/faculties.module";
import { Faculty } from "./faculties/faculty.entity";
import { Society } from "./societies/society.entity";
import { User } from "./users/user.entity";
import { FaqCategoriesModule } from "./faq/faq-categories/faq-categories.module";
import { FaqQuestionsModule } from "./faq/faq-questions/faq-questions.module";
import { FaqQuestion } from "./faq/faq-questions/faq-question.entity";
import { FaqCategory } from "./faq/faq-categories/faq-category.entity";
import { EventsModule } from "./events/events.module";
import { Event } from "./events/event.entity";
import { DashboardModule } from "./dashboard/dashboard.module";
import { MailerModule } from "@nestjs-modules/mailer";
import { join } from "path";
import { PugAdapter } from "@nestjs-modules/mailer/dist/adapters/pug.adapter";
import { AuthModule } from "./auth/auth.module";
import { FundingsModule } from "./fundings/fundings.module";
import { GrantsModule } from "./grants/grants.module";
import { AccountsModule } from "./accounts/accounts.module";
import { BudgetsModule } from "./budgets/budgets.module";
import { Account } from "./accounts/account.entity";
import { Budget } from "./budgets/budget.entity";
import { Funding } from "./fundings/funding.entity";
import { Grant } from "./grants/grant.entity";
import { EmailVerification } from "./auth/email-verification.entity";
import { LoginVerification } from "./auth/login-verification.entity";
import { APP_GUARD } from "@nestjs/core";
import { RolesGuard } from "./common/guards/roles.guard";
import { FilesModule } from "./files/files.module";
import { File } from "./files/file.entity";
import { utilities as nestWinstonModuleUtilities, WinstonModule } from 'nest-winston';
import * as winston from 'winston';
import * as path from "path";
import 'winston-daily-rotate-file';
import {LoggerMiddleware} from "./common/middlewares/logger.middleware";
import {AccountsController} from "./accounts/accounts.controller";
import {AuthController} from "./auth/auth.controller";
import {BudgetsController} from "./budgets/budgets.controller";
import {DashboardController} from "./dashboard/dashboard.controller";
import {DashboardUsersController} from "./dashboard/dashboard-users.controller";
import {DashboardRknController} from "./dashboard/dashboard-rkn.controller";
import {DashboardZcoController} from "./dashboard/dashboard-zco.controller";
import {EventsController} from "./events/events.controller";
import {FacultiesController} from "./faculties/faculties.controller";
import {FaqCategoriesController} from "./faq/faq-categories/faq-categories.controller";
import {FaqQuestionsController} from "./faq/faq-questions/faq-questions.controller";
import {FilesController} from "./files/files.controller";
import {FundingsController} from "./fundings/fundings.controller";
import {GrantsController} from "./grants/grants.controller";
import {SocietiesController} from "./societies/societies.controller";
import {UsersController} from "./users/users.controller";


const customFormat = winston.format.printf(({ level, message, timestamp, context, ...meta }) => {
    if (typeof message === 'undefined' && Object.keys(meta).length > 0)
        message = JSON.stringify(meta);
    return `${timestamp} | ${level.toUpperCase().padEnd(5)} | [${context}] ${message}`;
});

@Module({
    imports: [
        WinstonModule.forRoot({
            transports: [
                new winston.transports.Console({
                    format: nestWinstonModuleUtilities.format.nestLike(),
                    level: (process.env.NODE_ENV === 'development') ? 'debug' : 'info'
                }),
                new winston.transports.DailyRotateFile({
                    level: 'debug',
                    dirname: path.join(__dirname, './../logs/'),
                    filename: 'debug-%DATE%.log',
                    datePattern: 'YYYY-MM-DD',
                    createSymlink: true,
                    symlinkName: 'latest.log',
                    maxSize: '5m',
                    maxFiles: '30d',
                    format: winston.format.combine(
                        winston.format.timestamp({ format: 'YYYY/MM/DD HH:mm:ss' }),
                        customFormat
                    ),
                }),
            ],
        }),
        ConfigModule.forRoot(),
        MailerModule.forRoot({
            transport: {
                // service: 'gmail',
                host: 'smtp.gmail.com',
                port: 465,
                secure: true,
                auth: {
                    type: 'OAuth2',
                    user: 'donotreply@rkn.put.poznan.pl',
                    clientId: process.env.GMAIL_CLIENT_ID,
                    clientSecret: process.env.GMAIL_CLIENT_SECRET,
                    refreshToken: process.env.GMAIL_REFRESH_TOKEN,
                    accessToken: process.env.GMAIL_ACCESS_TOKEN
                },
            },
            preview: false,
            template: {
                dir: join(__dirname, '..', 'views', 'mail'),
                adapter: new PugAdapter({
                    inlineCssEnabled: true,
                    inlineCssOptions: {base_url: ' '},
                }),
                options: {
                    strict: true,
                },
            },
        }),
        TypeOrmModule.forRoot({
            type: 'mysql',
            host: process.env.DB_HOST,
            port: parseInt(process.env.DB_PORT),
            // host: 'localhost',
            // username: 'root',
            // password: '',
            username: process.env.DB_USERNAME,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME,
            entities: [
                Faculty,
                Society,
                User,
                EmailVerification,
                LoginVerification,
                FaqCategory,
                FaqQuestion,
                Event,
                Account,
                Budget,
                Funding,
                Grant,
                File
            ],
            synchronize: process.env.DB_SYNCHRONIZE === 'true',
        }),
        FacultiesModule,
        SocietiesModule,
        UsersModule,
        FaqCategoriesModule,
        FaqQuestionsModule,
        EventsModule,
        DashboardModule,
        AuthModule,
        FundingsModule,
        GrantsModule,
        AccountsModule,
        BudgetsModule,
        FilesModule,
    ],
    controllers: [AppController],
    providers: [AppService, {
        provide: APP_GUARD,
        useClass: RolesGuard,
    },],
})
export class AppModule implements NestModule {
    configure(consumer: MiddlewareConsumer): void {
        consumer.apply(LoggerMiddleware).forRoutes(
            AppController,
            AccountsController,
            AuthController,
            BudgetsController,
            DashboardController,
            DashboardUsersController,
            DashboardRknController,
            DashboardZcoController,
            EventsController,
            FacultiesController,
            FaqCategoriesController,
            FaqQuestionsController,
            FilesController,
            FundingsController,
            GrantsController,
            SocietiesController,
            UsersController
        );
    }
}

import { Injectable } from "@nestjs/common";
import { FacultiesService } from "./faculties/faculties.service";
import { SocietiesService } from "./societies/societies.service";
import { FaqCategoriesService } from "./faq/faq-categories/faq-categories.service";
import { EventsService } from "./events/events.service";
import { MailerService } from "@nestjs-modules/mailer";
import { UsersService } from "./users/users.service";
import { AuthService } from "./auth/auth.service";


@Injectable()
export class AppService {
    constructor(
        private facultiesService: FacultiesService,
        private societiesService: SocietiesService,
        private usersService: UsersService,
        private authService: AuthService,
        private faqCategoriesService: FaqCategoriesService,
        private eventsService: EventsService,
    ) {
    }

    async getFaculties() {
        const result = await this.facultiesService.findAll();
        const organizations = [], faculties = [];
        for (const item of result) {
            if (item['slug'] === 'organizacje-studenckie')
                organizations.push(item);
            else
                faculties.push(item);

        }
        return [organizations, faculties];
    }

    async getSocieties() {
        return await this.societiesService.findAll();
    }

    async getSociety(slug: string) {
        return await this.societiesService.findBySlug(slug);
    }

    async getFaculty(slug: string) {
        return await this.facultiesService.findBySlug(slug);
    }

    async getFaqCategories() {
        return await this.faqCategoriesService.findAll();
    }

    async getEvents() {
        return await this.eventsService.findAll();
    }

    async registerUser(data) {
        await this.usersService.register(data);
        await this.authService.registerUser(data.email);
    }

    async getUser(email: string) {
        return await this.usersService.findUser(email);
    }
}

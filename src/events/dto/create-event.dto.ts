import { IsNotEmpty } from "class-validator";
import { IsAcceptedPicture } from "../../common/decorators/picture-extension";

export class CreateEventDto {
    @IsNotEmpty()
    name!: string;

    @IsNotEmpty()
    description!: string;

    @IsNotEmpty()
    @IsAcceptedPicture()
    picture!: string;
}

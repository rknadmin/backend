import { Body, Controller, Delete, Get, Param, Patch, Post, UseFilters, UseGuards } from "@nestjs/common";
import { EventsService } from "./events.service";
import { CreateEventDto } from "./dto/create-event.dto";
import { UpdateEventDto } from "./dto/update-event.dto";
import { AuthExceptionFilter } from "../common/filters/auth-exceptions.filter";
import { AuthenticatedGuard } from "../common/guards/authenticated.guard";
import { Roles } from "../common/decorators/roles.decorator";

@Controller('api/events')
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class EventsController {
    constructor(private readonly eventsService: EventsService) {
    }

    @Get()
    @Roles('rkn')
    async findAll() {
        return await this.eventsService.findAll();
    }
}

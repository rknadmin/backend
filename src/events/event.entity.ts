import { Column, Entity, Unique } from "typeorm";
import { TimeStampedEntity } from "../common/entities/time-stamped.entity";
import { IsNotEmpty } from "class-validator";
import { IsAcceptedPicture } from "../common/decorators/picture-extension";

@Entity()
@Unique(['name'])
export class Event extends TimeStampedEntity {
    @Column()
    @IsNotEmpty()
    name: string;

    @Column('longtext')
    @IsNotEmpty()
    description: string;

    @Column()
    @IsNotEmpty()
    @IsAcceptedPicture()
    picture: string;
}

import { Injectable } from "@nestjs/common";
import { Repository } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";
import { Event } from "./event.entity";

@Injectable()
export class EventsService {
    constructor(@InjectRepository(Event) private eventsRepository: Repository<Event>) {
    }

    async findAll() {
        return await this.eventsRepository.createQueryBuilder('event')
            .orderBy('event.name')
            .getMany();
    }
}

import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  UploadedFile,
  UseFilters,
  UseGuards,
  UseInterceptors
} from "@nestjs/common";
import { SocietiesService } from "./societies.service";
import { UpdateSocietyDto } from "./dto/update-society.dto";
import { AuthExceptionFilter } from "../common/filters/auth-exceptions.filter";
import { AuthenticatedGuard } from "../common/guards/authenticated.guard";
import { Roles } from "../common/decorators/roles.decorator";
import { PatchSocietyDto } from "./dto/patch-society.dto";
import { FileInterceptor } from "@nestjs/platform-express";

@Controller("api/societies")
@UseFilters(AuthExceptionFilter)
@UseGuards(AuthenticatedGuard)
export class SocietiesController {
    constructor(private readonly societiesService: SocietiesService) {
    }

    @Post()
    @Roles('rkn', 'zco')
    create(@Body() createSocietyDto) {
        return this.societiesService.create(createSocietyDto);
    }

    @Get()
    @Roles('rkn', 'zco')
    findAll() {
        return this.societiesService.findAll();
    }

    @Get(":id")
    @Roles('rkn', 'zco')
    findOne(@Param("id") id: string) {
        return this.societiesService.findOne(+id);
    }

    @Put(":id")
    @Roles('rkn', 'zco')
    update(@Param("id") id: string, @Body() updateSocietyDto: UpdateSocietyDto) {
        return this.societiesService.update(+id, updateSocietyDto);
    }

    @Patch(':id/logo')
    @Roles('all')
    @UseInterceptors(FileInterceptor('file', { limits: {fileSize: 5000000} }))
    async updateLogo(@Param('id') id: string, @UploadedFile() file: Express.Multer.File) {
        return await this.societiesService.updateLogo(+id, file);
    }

    @Delete(":id/logo")
    @Roles('all')
    async removeLogo(@Param('id') id: string) {
        return await this.societiesService.removeLogo(+id);
    }

    @Patch(':id')
    @Roles('all')
    patch(@Param('id') id: string, @Body() patchSocietyDto: PatchSocietyDto) {
        return this.societiesService.patch(+id, patchSocietyDto);
    }

    @Patch('activate/:id')
    @Roles('rkn', 'zco')
    async activateUser(@Param('id') id: string) {
        return await this.societiesService.activate(+id);
    }

    @Patch('suspend/:id')
    @Roles('rkn', 'zco')
    async suspendUser(@Param('id') id: string) {
        return await this.societiesService.suspend(+id);
    }

    @Delete(":id")
    @Roles('rkn', 'zco')
    remove(@Param("id") id: string) {
        return this.societiesService.remove(+id);
    }
}

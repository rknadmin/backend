enum SocietyType {
    undefined = 'undefined',
    club = 'club',
    organization = 'organization',
}

export default SocietyType;
import { Column, Entity, ManyToMany, ManyToOne, OneToMany } from "typeorm";
import { Faculty } from "../faculties/faculty.entity";
import { TimeStampedSluggifiedEntity } from "../common/entities/time-stamped-sluggified.entity";
import { IsUrl } from "class-validator";
import SocietyType from "./enums/society-type.enum";
import { User } from "../users/user.entity";
import { Budget } from "../budgets/budget.entity";
import { Funding } from "../fundings/funding.entity";
import { Grant } from "../grants/grant.entity";
import { IsPutDomain } from "../common/decorators/email-domain.decorator";

@Entity()
export class Society extends TimeStampedSluggifiedEntity {
    @Column({nullable: true})
    abbreviation: string;

    @Column({nullable: true})
    @IsPutDomain()
    email: string;

    @Column({nullable: true})
    @IsPutDomain()
    emailSecondary: string;

    @Column()
    leader: string;

    @Column()
    supervisor: string;

    @Column({nullable: true})
    @IsPutDomain()
    emailSupervisor: string;

    @Column({nullable: true})
    @IsUrl()
    website: string;

    @Column({nullable: true})
    @IsUrl()
    facebook: string;

    @Column({nullable: true})
    instagram: string;

    @Column('longtext', {nullable: true})
    description: string;

    @Column({default: SocietyType.undefined})
    type: SocietyType;

    @Column({default: true})
    visible: boolean;

    @Column({default: true})
    active: boolean;

    @Column({nullable: true})
    logoPath: string;

    @ManyToOne((type) => Faculty, (faculty) => faculty.societies, {
        onDelete: 'CASCADE',
    })
    faculty: Faculty;

    @ManyToMany(() => User)
    users: User[];

    @OneToMany((type) => Budget, (budget) => budget.society, {eager: true})
    budgets: Budget[];

    @OneToMany((type) => Funding, (funding) => funding.society, {eager: true})
    fundings: Funding[];

    @OneToMany((type) => Grant, (grant) => grant.society, {eager: true})
    grants: Grant[];
}

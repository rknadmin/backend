import { Injectable, InternalServerErrorException } from "@nestjs/common";
import { CreateSocietyDto } from "./dto/create-society.dto";
import { UpdateSocietyDto } from "./dto/update-society.dto";
import { InjectRepository } from "@nestjs/typeorm";
import { Society } from "./society.entity";
import { Repository } from "typeorm";
import { FacultiesService } from "../faculties/faculties.service";
import SocietyType from "./enums/society-type.enum";
import { PatchSocietyDto } from "./dto/patch-society.dto";
import { MailerService } from "@nestjs-modules/mailer";
import { FilesService } from "../files/files.service";
import EntityType from "../files/enums/entity-type.enum";

@Injectable()
export class SocietiesService {
    constructor(
        @InjectRepository(Society) private societiesRepository: Repository<Society>,
        private facultiesService: FacultiesService,
        private filesService: FilesService,
        private readonly mailerService: MailerService
    ) {
    }

    async create(createSocietyDto: CreateSocietyDto) {
        const {facultyId, ...data} = createSocietyDto;
        const society = await this.societiesRepository.create(data);
        const faculty = await this.facultiesService.findOne(facultyId);
        society.faculty = faculty;
        if (faculty.name === 'Organizacje Studenckie')
            society.type = SocietyType.organization;
        else
            society.type = SocietyType.club;
        return this.saveHelper(society);
    }

    async findAll() {
        return await this.societiesRepository.createQueryBuilder('society')
            .leftJoinAndSelect('society.faculty', 'faculty')
            .getMany();
    }

    async findAllWithBudgets() {
        return await this.societiesRepository.createQueryBuilder('society')
            .leftJoinAndSelect('society.budgets', 'budget')
            .leftJoinAndSelect('budget.account', 'account')
            .getMany();
    }

    async findOne(id: number) {
        return await this.societiesRepository.createQueryBuilder('society')
            .where('society.id = :id', { id })
            .leftJoinAndSelect('society.faculty', 'faculty')
            .leftJoinAndSelect('society.budgets', 'budget')
            .leftJoinAndSelect('budget.account', 'account')
            .getOne();
    }

    async findBySlug(slug: string) {
        // return await this.societiesRepository.findOne({slug}, {relations: ['faculty', 'budgets', 'budgets.account', 'fundings', 'fundings.budget', 'fundings.budget.account']});
        return await this.societiesRepository.createQueryBuilder('society')
            .where('society.slug = :slug', {slug})
            .leftJoinAndSelect('society.faculty', 'faculty')
            .leftJoinAndSelect('society.budgets', 'budget')
            .leftJoinAndSelect('budget.account', 'account')
            .leftJoinAndSelect('society.fundings', 'funding')
            .leftJoinAndSelect('funding.budget', 'fundingBudget')
            .leftJoinAndSelect('fundingBudget.account', 'fundingBudgetAccount')
            .leftJoinAndSelect('society.grants', 'grant')
            .leftJoinAndSelect('grant.budget', 'grantBudget')
            .leftJoinAndSelect('grantBudget.account', 'grantBudgetAccount')
            .getOne();
    }

    async update(id: number, updateSocietyDto: UpdateSocietyDto) {
        const society = await this.findOne(id);
        const {facultyId, ...data} = updateSocietyDto;
        for (const prop of ['abbreviation', 'email', 'emailSecondary', 'emailSupervisor', 'website', 'facebook', 'instagram'])
            society[prop] = null;
        for (const prop in data) society[prop] = data[prop];

        if (facultyId) {
            const faculty = await this.facultiesService.findOne(facultyId);
            society.faculty = faculty;

            if (faculty.name === 'Organizacje Studenckie')
                society.type = SocietyType.organization;
            else
                society.type = SocietyType.club;
        }

        return this.saveHelper(society);
    }

    async updateLogo(id: number, logo: Express.Multer.File) {
        const file = await this.filesService.create(logo, EntityType.Society, id);
        const society = await this.findOne(id);
        society.logoPath = file.path;
        await society.save();
        return file;
    }

    async removeLogo(id: number) {
        const society = await this.findOne(id);
        society.logoPath = null;
        return await this.saveHelper(society);
    }

    async patch(id: number, patchSocietyDto: PatchSocietyDto) {
        const society = await this.findOne(id);
        for (const prop in patchSocietyDto) society[prop] = patchSocietyDto[prop]
        return this.saveHelper(society);
    }

    async activate(id: number) {
        const society = await this.societiesRepository.createQueryBuilder('society')
            .where('id = :id', { id })
            .getOne();
        society.active = true;
        await this.saveHelper(society).catch(err => { throw new InternalServerErrorException(err) });

        let emails = [];
        // society.users.forEach(user => emails.push(user.email));
        emails.push(society.email);
        emails.push(society.emailSecondary);
        emails.push(society.emailSupervisor);
        emails = [...new Set(emails)];

        const recipients = emails.join(',');

        await this.mailerService.sendMail({
            to: recipients,
            from: 'RKNAdmin <donotreply@rkn.put.poznan.pl>',
            subject: '[RKNAdmin] Aktywacja jednostki',
            template: 'user',
            context: {
                header: 'Aktywacja jednostki',
                message: `Jednostka ${society.name} została ponownie aktywowana.`
            }
        }).then(() => {
            return society;
        }).catch(() => {
            throw new InternalServerErrorException('Wystąpił błąd podczas wysyłania wiadomości e-mail');
        });
    }

    async suspend(id: number) {
        const society = await this.societiesRepository.createQueryBuilder('society')
            .where('id = :id', { id })
            .getOne();
        society.active = false;
        await this.saveHelper(society).catch(err => { throw new InternalServerErrorException(err) });

        let emails = [];
        emails.push(society.email);
        emails.push(society.emailSecondary);
        emails.push(society.emailSupervisor);
        emails = [...new Set(emails)];

        const recipients = emails.join(',');

        await this.mailerService.sendMail({
            to: recipients,
            from: 'RKNAdmin <donotreply@rkn.put.poznan.pl>',
            subject: '[RKNAdmin] Zawieszenie jednostki',
            template: 'user',
            context: {
                header: 'Zawieszenie jednostki',
                message: `Jednostka ${society.name} została zawieszona.`
            }
        }).then(() => {
            return society;
        }).catch(() => {
            throw new InternalServerErrorException('Wystąpił błąd podczas wysyłania wiadomości e-mail');
        });
    }

    async remove(id: number) {
        return await this.societiesRepository.delete(id);
    }

    async saveHelper(society: Society) {
        try {
            return await society.save();
        } catch (err) {
            if (err.errno === 1062)
                throw new InternalServerErrorException(
                    `Society with name "${society.name}" or slug "${society.slug}" already exists.`,
                );
            else throw new InternalServerErrorException();
        }
    }
}

import { IsNotEmpty } from "class-validator";
import { IsPutDomain } from "../../common/decorators/email-domain.decorator";

export class CreateSocietyDto {
    @IsNotEmpty()
    facultyId!: number;

    @IsNotEmpty()
    name!: string;

    abbreviation?: string;
    leader!: string;
    supervisor!: string;

    @IsPutDomain()
    email!: string;

    @IsPutDomain()
    emailSecondary?: string;

    @IsPutDomain()
    emailSupervisor?: string;

    website?: string;
    facebook?: string;
    instagram?: string;
    description?: string;
}

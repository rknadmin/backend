import { IsPutDomain } from "../../common/decorators/email-domain.decorator";

export class UpdateSocietyDto {
    facultyId?: number;
    name?: string;
    abbreviation?: string;

    @IsPutDomain()
    email?: string;

    @IsPutDomain()
    emailSecondary?: string;

    @IsPutDomain()
    emailSupervisor?: string;

    leader?: string;
    supervisor?: string;
    website?: string;
    facebook?: string;
    instagram?: string;
    description?: string;
}

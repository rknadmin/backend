import { IsPutDomain } from "../../common/decorators/email-domain.decorator";

export class PatchSocietyDto {
    @IsPutDomain()
    email?: string;

    @IsPutDomain()
    emailSecondary?: string;

    website?: string;
    facebook?: string;
    instagram?: string;
    description?: any;
}

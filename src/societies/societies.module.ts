import { forwardRef, Module } from "@nestjs/common";
import { SocietiesService } from "./societies.service";
import { SocietiesController } from "./societies.controller";
import { TypeOrmModule } from "@nestjs/typeorm";
import { Society } from "./society.entity";
import { FacultiesModule } from "../faculties/faculties.module";
import { FilesModule } from "../files/files.module";

@Module({
    imports: [TypeOrmModule.forFeature([Society]), FacultiesModule, forwardRef(() => FilesModule)],
    controllers: [SocietiesController],
    providers: [SocietiesService],
    exports: [SocietiesService],
})
export class SocietiesModule {
}

// NAME AUTOFILL

const emailInput = $('#email');
const nameInput = $('#name');
const submitBtn = $('button[type="submit"]');

emailInput.on('input', () => {
  let arr = emailInput.val().split('.');

  for (let i = 0; i < arr.length; i++)
    arr[i] = arr[i].charAt(0).toUpperCase() + arr[i].slice(1);

  let value;

  if (arr.length === 1) {
    submitBtn.prop("disabled", true);
    value = `${arr[0]}`;
  } else if (arr.length === 2 && arr[1].length > 0) {
    submitBtn.removeClass('disabled');
    submitBtn.prop("disabled", false);
    value = `${arr[0]} ${arr[1]}`;
  } else if (arr.length === 3 && arr[2].length > 0) {
    submitBtn.removeClass('disabled');
    submitBtn.prop("disabled", false);
    value = `${arr[0]} ${arr[2]}`;
  } else {
    submitBtn.prop("disabled", true);
    value = 'email nieprawidłowy';
  }

  if(arr.some(el => el.includes('@'))) {
    submitBtn.prop("disabled", true);
    value = 'email nieprawidłowy';
  }

  nameInput.val(value);
});
const dropzonePL = {
    /**
     * The text used before any files are dropped.
     */
    dictDefaultMessage: "Kliknij i wybierz pliki lub przeciągnij i upuść tutaj",

    /**
     * The text that replaces the default message text it the browser is not supported.
     */
    dictFallbackMessage: "Twoja przeglądarka nie wspiera przeciągania i upuszczania plików.",

    /**
     * The text that will be added before the fallback form.
     * If you provide a  fallback element yourself, or if this option is `null` this will
     * be ignored.
     */
    dictFallbackText: "Użyj poniższego formularza by wgrać pliki.",

    /**
     * If the filesize is too big.
     * `{{filesize}}` and `{{maxFilesize}}` will be replaced with the respective configuration values.
     */
    dictFileTooBig: "Plik jest zbyt duży ({{filesize}}MiB). Maksymalny rozmiar pliku: {{maxFilesize}}MiB.",

    /**
     * If the file doesn't match the file type.
     */
    dictInvalidFileType: "Nie możesz wgrać plików tego typu.",

    /**
     * If the server response was invalid.
     * `{{statusCode}}` will be replaced with the servers status code.
     */
    dictResponseError: "Kod odpowiedzi serwera: {{statusCode}}.",

    /**
     * If `addRemoveLinks` is true, the text to be used for the cancel upload link.
     */
    dictCancelUpload: "Przerwij wgrywanie",

    /**
     * The text that is displayed if an upload was manually canceled
     */
    dictUploadCanceled: "Wgrywanie przerwane.",

    /**
     * If `addRemoveLinks` is true, the text to be used for confirmation when cancelling upload.
     */
    dictCancelUploadConfirmation: "Jesteś pewien, że chcesz przerwać wgrywanie?",

    /**
     * If `addRemoveLinks` is true, the text to be used to remove a file.
     */
    dictRemoveFile: "Usuń plik",

    /**
     * If this is not null, then the user will be prompted before removing a file.
     */
    dictRemoveFileConfirmation: null,

    /**
     * Displayed if `maxFiles` is st and exceeded.
     * The string `{{maxFiles}}` will be replaced by the configuration value.
     */
    dictMaxFilesExceeded: "Nie możesz wgrać więcej plików.",
}